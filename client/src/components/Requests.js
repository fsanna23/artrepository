/*
This software is distributed under MIT/X11 license

Copyright (c) 2021 Federico Sanna - University of Cagliari

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

import React, { useState, useEffect } from "react";
import { pages } from "./PageEnum";

function Requests(props) {
  const [state, setState] = useState({});
  const [users, setUsers] = useState([]);

  useEffect(() => {
    (async function () {
      await updateRequests();
    })();
  }, []);

  useEffect(() => {
    (async function () {
      if (props.appState === pages.REQUESTS) {
        console.log("Updated");
        await updateRequests();
      }
    })();
  }, [props.appState]);

  const updateRequests = async () => {
    const checkCertified = (artId) => {
      if (props.art && props.certArt) {
        for (let i = 0; i < props.art.length; i++) {
          if (props.art[i].id === artId) {
            return false;
          }
        }
        for (let i = 0; i < props.certArt.length; i++) {
          if (props.certArt[i].id === artId) {
            return true;
          }
        }
      }
    };

    console.log("Updating");

    if (props.userData) {
      console.log(props.web3.accounts);
      console.log(props.web3.web3.eth.defaultAccount);
      const requestArray = await props.web3.contract.methods
        .getRequests()
        .call({ from: props.web3.web3.eth.defaultAccount });
      console.log(requestArray);
      let requests = [];
      for (let i = 0; i < requestArray[0].length; i++) {
        const isCert = checkCertified(requestArray[0][i]);
        requests.push({
          art: requestArray[0][i],
          user: requestArray[1][i],
          proposedPrice: requestArray[2][i],
          isCertified: isCert,
        });
      }
      // Get users if not already taken
      const uniqueUsers = [...new Set(requestArray[1])];
      for (const user of uniqueUsers) {
        if (users.filter((el) => el.address === user).length === 0) {
          const userData = await props.web3.contract.methods
            .getUserData(user)
            .call();
          let newUsers = users;
          newUsers.push({ address: user, name: userData[0] });
          setUsers(newUsers);
        }
      }
      // Finalize
      // props.setRequests(requests);
      setState(requests);
      /*if (!props.userData.requests) {
        const requestArray = await props.web3.contract.methods
          .getRequests()
          .call();
        console.log(requestArray);
        let requests = [];
        for (let i = 0; i < requestArray[0].length; i++) {
          const isCert = checkCertified(requestArray[0][i]);
          requests.push({
            art: requestArray[0][i],
            user: requestArray[1][i],
            proposedPrice: requestArray[2][i],
            isCertified: isCert,
          });
        }
        // Get users if not already taken
        const uniqueUsers = [...new Set(requestArray[1])];
        for (const user of uniqueUsers) {
          if (users.filter((el) => el.address === user).length === 0) {
            const userData = await props.web3.contract.methods
              .getUserData(user)
              .call();
            let newUsers = users;
            newUsers.push({ address: user, name: userData[0] });
            setUsers(newUsers);
          }
        }
        // Finalize
        props.setRequests(requests);
        setState(requests);
      } else {
        setState(props.userData.requests);
      }*/
    }
  };

  const getArtTitle = (artId) => {
    if (props.art && props.certArt) {
      for (let i = 0; i < props.art.length; i++) {
        if (props.art[i].id === artId) {
          return props.art[i].title;
        }
      }
      for (let i = 0; i < props.certArt.length; i++) {
        if (props.certArt[i].id === artId) {
          return props.certArt[i].title;
        }
      }
    }
  };

  const getArt = (art) => {
    if (art.isCertified) {
      for (let i = 0; i < props.certArt.length; i++) {
        if (props.certArt[i].id === art.art) {
          return props.certArt[i];
        }
      }
    } else {
      for (let i = 0; i < props.art.length; i++) {
        if (props.art[i].id === art.art) {
          return props.art[i];
        }
      }
    }
  };

  const onShowArt = (art) => {
    let fullArt = getArt(art);
    let type;
    if (art.isCert) {
      type = "certified";
    } else {
      type = "owned";
    }
    props.setViewArt({ art: fullArt, type: type });
  };

  const onSellArt = async (art) => {
    await props.web3.contract.methods
      .acceptRequest(art.art, art.user)
      .send({ from: props.web3.accounts[0] });
    await updateRequests();
    // Update app
    await props.updateApp(null, true);
  };

  const getName = (address) => {
    return users.find((user) => user.address === address).name;
  };

  return (
    <div>
      <h3 className="h3 mb-3">Le richieste alle tue opere</h3>
      {Array.isArray(state) && state.length !== 0 ? (
        state.map((el) => {
          return (
            <div className="card text-left" key={el.art + "-" + el.user}>
              <h5 className="card-header">
                Offerta per l'opera: {getArtTitle(el.art)}
              </h5>
              <span className="card-body">
                <p>
                  L'offerente {getName(el.user)} ha proposto un prezzo di €
                  {el.proposedPrice}
                </p>
                <a
                  href="#"
                  className="btn btn-primary"
                  onClick={() => {
                    onShowArt(el);
                  }}
                >
                  Mostra opera
                </a>
                <a
                  href="#"
                  className="btn btn-success ml-2"
                  onClick={() => {
                    onSellArt(el);
                  }}
                >
                  Vendi opera
                </a>
              </span>
            </div>
          );
        })
      ) : (
        <p>Non hai nessuna richiesta</p>
      )}
    </div>
  );
}

export default Requests;
