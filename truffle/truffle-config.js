/*
This software is distributed under MIT/X11 license

Copyright (c) 2021 Federico Sanna - University of Cagliari

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

require("dotenv").config();
const HDWalletProvider = require("@truffle/hdwallet-provider");

module.exports = {
  compilers: {
    solc: {
      version: "0.6.2",
      optimizer: {
        enabled: true,
        runs: 200,
      },
    },
  },
  plugins: ["truffle-plugin-verify"],
  contracts_build_directory: "../client/src/contracts",
  networks: {
    ganache: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "",
    },
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*",
    },
    test: {
      host: "127.0.0.1",
      port: 7545,
      network_id: 5777,
    },
    kovan: {
      provider: () =>
        new HDWalletProvider({
          mnemonic: {
            phrase: process.env.MNEMONIC,
          },
          providerOrUrl:
            "https://kovan.infura.io/v3/84d54bb37ff646dca4c97cc45593c61d",
          numberOfAddresses: 1,
        }),
    },
    goerli: {
      provider: () =>
        new HDWalletProvider({
          mnemonic: {
            phrase: process.env.MNEMONIC,
          },
          providerOrUrl:
            "wss://eth-goerli.ws.alchemyapi.io/v2/OF1cxi-DGoT2grirkEZNiLKY0KfBEDdR",
          numberOfAddresses: 1,
          shareNonce: true,
        }),
      network_id: 5,
    },
    ropsten: {
      provider: () =>
        new HDWalletProvider({
          mnemonic: {
            phrase: process.env.MNEMONIC,
          },
          providerOrUrl:
            "https://ropsten.infura.io/v3/84d54bb37ff646dca4c97cc45593c61d",
          numberOfAddresses: 1,
          shareNonce: true,
        }),
      network_id: 3,
    },
  },
  api_keys: {
    etherscan: process.env.API_KEY,
  },
};
