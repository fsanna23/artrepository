/*
This software is distributed under MIT/X11 license

Copyright (c) 2021 Federico Sanna - University of Cagliari

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

import axios from "axios";

const getImages = (array, iter, params) => {
  return new Promise((resolve, reject) => {
    let fileName = params.id;
    if (iter !== 1) {
      fileName += `_${iter}`;
    }
    axios
      .get("http://localhost:9000/getimg", {
        params: { fileName: fileName + ".jpg" },
        headers: { authorization: params.token },
      })
      .then((res) => {
        let arrayBufferView = new Uint8Array(res.data.file.data);
        let blob = new Blob([arrayBufferView], { type: "image/jpeg" });
        array.push(blob);
        if (iter !== params.numImages) {
          resolve(getImages(array, iter + 1, params));
        } else {
          resolve(array);
        }
      })
      .catch((err) => {
        console.log(err);
        reject(err);
      });
  });
};

export const recGetImages = async function (params) {
  return new Promise((resolve, reject) => {
    getImages([], 1, params)
      .then((res) => {
        console.log(res);
        resolve(res);
      })
      .catch((err) => {
        console.log(err);
        reject(false);
      });
  });
};
