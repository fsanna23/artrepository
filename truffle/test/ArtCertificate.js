/*
This software is distributed under MIT/X11 license

Copyright (c) 2021 Federico Sanna - University of Cagliari

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

const { expectRevert } = require("@openzeppelin/test-helpers");

const ArtCertificate = artifacts.require("ArtCertificate");

contract("ArtCertificate", (accounts) => {
  let instance;

  beforeEach(async () => {
    instance = await ArtCertificate.deployed();
  });

  it("should insert new user correctly", async () => {
    let users = [
      "0x62CeeD6A2e86f1a4bA54cc7E9C1C1d61E229109a",
      "0xaFdB5c9612c0961Bb7f577202F5888d009ca84B0",
      "0x62CeeD6A2e86f1a4bA54cc7E9C1C1d61E229109a",
    ];
    let names = ["Jack", "Mark", "Thomas"];
    let ids = ["1", "2", "3"];

    await instance.fillUser(users, names, ids, { from: accounts[0] });
    const userData = await instance.getUserData(users[2]);
    assert.equal(userData[0], names[0], "The name is not correct");
    assert.notEqual(userData[0], names[2], "The name is not correct");
  });

  it("should associate user to the right art pieces", async () => {
    const userArt = await instance.getUserArt({ from: accounts[0] });
    const userArtIds = userArt[0].map((item) => item.words[0]);
    assert.equal(
      userArtIds[0],
      1,
      "The user does not own the right art pieces"
    );
    assert.equal(
      userArtIds[1],
      3,
      "The user does not own the right art pieces"
    );
    assert.equal(
      userArt[1].length,
      0,
      "The user does not own the right art pieces"
    );
    assert.equal(
      userArt[2].length,
      0,
      "The user does not own the right art pieces"
    );
    assert.equal(
      userArt[3].length,
      0,
      "The user does not own the right art pieces"
    );
  });

  it("should have no requests right after contract construction", async () => {
    const requests = await instance.getRequests({ from: accounts[0] });
    assert.equal(
      requests[0].length,
      0,
      "The user has requests for its art but he shouldn't"
    );
    assert.equal(
      requests[1].length,
      0,
      "The user has requests for its art but he shouldn't"
    );
    assert.equal(
      requests[2].length,
      0,
      "The user has requests for its art but he shouldn't"
    );
  });

  it("should revert when art requested is owned", async () => {
    await expectRevert(
      instance.requestArt(1, 100, { from: accounts[0] }),
      "The user owns the art"
    );
  });

  it("should update contract data when request is made", async () => {
    await instance.requestArt(2, 100, { from: accounts[0] });
    const requests = await instance.getRequests({ from: accounts[1] });
    assert.equal(
      requests[0].length,
      1,
      "The array of requested ids' length is wrong"
    );
    assert.equal(
      requests[1].length,
      1,
      "The array of requester addresses' length is wrong"
    );
    assert.equal(requests[2].length, 1, "The array of prices' length is wrong");
    assert.equal(
      requests[0][0].toString(),
      2,
      "The array of prices' length is wrong"
    );
    assert.equal(
      requests[1][0],
      accounts[0],
      "The array of prices' length is wrong"
    );
    assert.equal(
      requests[2][0].toString(),
      100,
      "The array of prices' length is wrong"
    );
    const userData = await instance.getUserData(accounts[1], {
      from: accounts[1],
    });
    assert.equal(userData[1], 1, "The number of requests is wrong");
  });

  it("should revert when art is requested more than once", async () => {
    await expectRevert(
      instance.requestArt(2, 100, { from: accounts[0] }),
      "You already requested this art"
    );
  });

  it("should revert if user accepts request for art not owned", async () => {
    await expectRevert(
      instance.acceptRequest(2, accounts[0], { from: accounts[0] }),
      "The user is not the owner"
    );
  });

  it("should revert if user accepts request for art not requested", async () => {
    await expectRevert(
      instance.acceptRequest(3, accounts[1], { from: accounts[0] }),
      "The art has not been requested"
    );
  });

  it("should update data when request is accepted", async () => {
    await instance.acceptRequest(2, accounts[0], { from: accounts[1] });
    const buyerArt = await instance.getUserArt({ from: accounts[0] });
    const sellerArt = await instance.getUserArt({ from: accounts[1] });
    assert.equal(
      buyerArt[0].length,
      3,
      "The buyer hasn't got the right number of art"
    );
    assert.equal(
      buyerArt[1].length,
      0,
      "The buyer hasn't got the right number of certified art"
    );
    assert.equal(
      buyerArt[2].length,
      1,
      "The buyer hasn't got the right number of art with prices"
    );
    assert.equal(
      buyerArt[3].length,
      1,
      "The buyer hasn't got the right number of prices"
    );
    assert.equal(
      sellerArt[0].length,
      0,
      "The seller hasn't got the right number of art"
    );
    assert.equal(
      sellerArt[1].length,
      0,
      "The seller hasn't got the right number of certified art"
    );
    assert.equal(
      sellerArt[2].length,
      1,
      "The seller hasn't got the right number of art with prices"
    );
    assert.equal(
      sellerArt[3].length,
      1,
      "The seller hasn't got the right number of prices"
    );
    assert.equal(
      buyerArt[0][buyerArt[0].length - 1],
      2,
      "The buyer hasn't got the right id for the art he just bought"
    );
    assert.equal(
      buyerArt[2][buyerArt[2].length - 1],
      2,
      "The art that has been bought doesn't appear in the prices array"
    );
    assert.equal(
      buyerArt[3][buyerArt[3].length - 1],
      100,
      "The price is not correct"
    );
    const userData = await instance.getUserData(accounts[1], {
      from: accounts[1],
    });
    assert.equal(userData[1].words[0], 0, "The number of requests is wrong");
  });

  it("should revert if user tries to create a token for a not owned art", async () => {
    await expectRevert(
      instance.addToken(
        2,
        "Name",
        "Description",
        10,
        20,
        100,
        "Jack",
        "0x62CeeD6A2e86f1a4bA54cc7E9C1C1d61E229109a",
        await web3.utils.asciiToHex("Hash"),
        "Link",
        { from: accounts[2] }
      ),
      "The user is not the owner"
    );
  });

  it("should create a new token and update data", async () => {
    await instance.addToken(
      1,
      "Name",
      "Description",
      10,
      20,
      100,
      "Jack",
      "0x62CeeD6A2e86f1a4bA54cc7E9C1C1d61E229109a",
      await web3.utils.asciiToHex("Hash"),
      "Link",
      { from: accounts[0] }
    );
    const userArt = await instance.getUserArt({ from: accounts[0] });
    assert.equal(
      userArt[1].toString(),
      1,
      "The art that has been certified is not present in the array"
    );
  });

  it("should transfer the token when a certified art is bought", async () => {
    await instance.requestArt(1, 200, { from: accounts[1] });
    await instance.acceptRequest(1, accounts[1], { from: accounts[0] });
    const userArt = await instance.getUserArt({ from: accounts[1] });
    assert.equal(
      userArt[1].toString(),
      1,
      "The art that has been transferred is not present in the array"
    );
  });
});
