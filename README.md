Advanced Programming Technique project files

The project is split into three different folders.

The client folder contains all the user interface parts of the project, and the migrations files created by Truffle and then used by web3.js to manage the contract's public functions.

The server folder contains an Express server which manages the Dropbox API access, the JSON file containing the art details and the locally stored images.

The truffle folder contains the Truffle configuration file, the solidity contracts and the migration files.