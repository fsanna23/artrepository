/*
This software is distributed under MIT/X11 license

Copyright (c) 2021 Federico Sanna - University of Cagliari

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

require("dotenv").config({ silent: true }); // read values from .env file

const handlebars = require("express-handlebars");
const express = require("express");
const path = require("path");
const controller = require("./controller");
const PORT = process.env.PORT || 9000;
const logger = require("morgan");
const session = require("express-session");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");

const app = express();

app.set("view engine", "handlebars");
app.engine(
  "handlebars",
  handlebars({
    layoutsDir: __dirname + "/views/layouts",
  })
);

app.use(logger("dev"));
app.use(express.static(path.join(__dirname, "public")));
app.use(cors());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
  })
);

app.get("/", controller.home); // home route
app.get("/auth", controller.auth); // OAuth redirect route
app.get("/getimg", controller.getImg); // get images from local
app.post("/createpdf", controller.createPdf); // generate pdf file
app.get("/getpdfstatus", controller.getPdfStatus); // get pdf status
app.get("/getgeneratedpdf", controller.getGeneratedPdf); // get pdf link
app.get("/getpdflink", controller.getPdfLink);
app.get("/getpdfhash", controller.getPdfHash);
app.get("/getartjson", controller.getArtJson);
app.get("/checktoken", controller.checkToken);
app.post("/setartjson", controller.setArtJson);
app.post("/downloadimg", controller.downloadImg);
app.post("/verifyhash", controller.verifyHash);

app.listen(PORT, () => console.log(`Server started in port ${PORT}`));
