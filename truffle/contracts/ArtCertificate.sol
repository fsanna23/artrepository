/*
This software is distributed under MIT/X11 license

Copyright (c) 2021 Federico Sanna - University of Cagliari

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/


pragma solidity >=0.6.2 <0.8.0;
pragma experimental ABIEncoderV2; // Used for names array in fillUser

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
//import "@openzeppelin/contracts/math/SafeMath.sol";

contract ArtCertificate is ERC721 {

    using SafeMath for uint256;

    /*  Maps an item index to the address of the owner */
    mapping(uint256 => address) private artToOwner;

    /*  Maps an item index to its last price at which it's been bought */
    mapping(uint256 => uint256) private artToPrice;

    /*  Maps the address to a User struct containing its data */
    mapping(address => User) private userList;

    /*  Maps an item index to a Request structure.*/
    mapping(uint256 => Request) private transferRequests;

    /*  Maps an item index to each token, if that item has been certified.*/
    mapping(uint256 => ArtToken) private tokenList;

    uint256[] private artWithPrices;

    bool private isConstructed;

    struct ArtToken {
        string name;
        string description;
        uint32 minExtimatedValue;
        uint32 maxExtimatedValue;
        uint32 price;
        string certifierName;
        address certifierAddress;
        bytes32 pdfHash;
        string pdfLink;
    }

    struct User {
        address userAddress;
        string userName;
        uint256 requestsNum;
        uint256[] ownedArt;
    }

    struct Request {
        uint256 itemId;
        address[] users;
        uint256[] proposedPrices;
    }

    modifier onlyOwner(uint256 _id) {
        require(msg.sender == artToOwner[_id], "The user is not the owner");
        _;
    }

    modifier notOwner(uint256 _id) {
        require(msg.sender != artToOwner[_id], "The user owns the art");
        _;
    }

    modifier isRequested(uint256 _id, address _user) {
        require(checkRequest(_user, _id), "The art has not been requested");
        _;
    }

    constructor() ERC721("ArtToken", "ARTTKN") public {
        isConstructed = false;
    }

    function checkRequest(address _buyer, uint256 _id)
    private
    view
    returns (bool)
    {
        address[] storage users = transferRequests[_id].users;
        for (uint256 i = 0; i < users.length; i++) {
            if (users[i] == _buyer) {
                return true;
            }
        }
        return false;
    }

    function checkConstructed() public view returns (bool) {
        return isConstructed;
    }

    function fillUser(
        address[] memory _users,
        string[] memory _names,
        uint256[] memory _artIds
    ) public {
        require(isConstructed == false);
        for (uint256 i = 0; i < _users.length; i++) {
            if (!checkExistingUser(_users[i])) {
                insertNewUser(_users[i], _names[i]);
            }
            artToOwner[_artIds[i]] = _users[i];
            userList[_users[i]].ownedArt.push(_artIds[i]);
        }
        isConstructed = true;
    }

    function addToken(
        uint256 _id,
        string memory _name,
        string memory _description,
        uint32 _miEv,
        uint32 _maEv,
        uint32 _price,
        string memory _certifierName,
        address _certifierAddress,
        bytes32 _pdfHash,
        string memory _pdfLink
    ) public onlyOwner(_id) {
        tokenList[_id] = ArtToken(
            _name,
            _description,
            _miEv,
            _maEv,
            _price,
            _certifierName,
            _certifierAddress,
            _pdfHash,
            _pdfLink
        );
        _mint(msg.sender, _id);
    }


    function checkExistingUser(address _user) public view returns (bool) {
        if (userList[_user].userAddress == _user) {
            return true;
        } else {
            return false;
        }
    }

    function insertNewUser(address _user, string memory _name) public {
        uint256[] memory tempList;
        userList[_user] = User(_user, _name, 0, tempList);
    }

    function getUserData(address _userId)
    public
    view
    returns (string memory name, uint256 requestsNum)
    {
        require(userList[_userId].userAddress == _userId);
        return (userList[_userId].userName, userList[_userId].requestsNum);
    }

    function getUserArt()
    public
    view
    returns (
        uint256[] memory,
        uint256[] memory,
        uint256[] memory,
        uint256[] memory
    )
    {
        uint256[] memory certifiedArt;
        uint256[] memory prices = getSoldArtPrices();
        if (balanceOf(msg.sender) != 0) {
            // The user owns certified art
            certifiedArt = getCertifiedArt(msg.sender, userList[msg.sender].ownedArt);
        }
        return (
        userList[msg.sender].ownedArt,
        certifiedArt,
        artWithPrices,
        prices
        );
    }

    function getCertifiedArt(address _user, uint256[] memory _ownedArt)
    private
    view
    returns (uint256[] memory)
    {
        uint256 counter = 0;
        uint256[] memory certifiedArt = new uint256[](balanceOf(_user));
        for (uint256 i = 0; i < _ownedArt.length; i++) {
            if (_exists(_ownedArt[i])) {
                if (ownerOf(_ownedArt[i]) == _user) {
                    certifiedArt[counter] = _ownedArt[i];
                    counter++;
                }
            }
        }
        return certifiedArt;
    }

    function getSoldArtPrices()
    private
    view
    returns (uint256[] memory)
    {
        uint256 counter = 0;
        uint256[] memory prices = new uint256[](artWithPrices.length);
        for (uint256 i = 0; i < artWithPrices.length; i++) {
            prices[counter] = artToPrice[artWithPrices[i]];
            counter++;
        }
        return prices;
    }

    function getCertifiedArtData(uint256 _id) public view returns(string memory, address, string memory, bytes32) {
        require(ownerOf(_id) == msg.sender);`○
        return (tokenList[_id].certifierName, tokenList[_id].certifierAddress, tokenList[_id].pdfLink, tokenList[_id].pdfHash);
    }

    function getHash(uint256 _id) public view returns(bytes32) {
        return (tokenList[_id].pdfHash);
    }

    function acceptRequest(uint256 _artId, address _user)
    public
    onlyOwner(_artId)
    isRequested(_artId, _user)
    {
        uint256[] memory tempList = new uint256[](userList[msg.sender].ownedArt.length - 1);
        uint256 counter = 0;
        for (uint256 i = 0; i < userList[msg.sender].ownedArt.length; i++) {
            if (userList[msg.sender].ownedArt[i] != _artId) {
                tempList[counter] = userList[msg.sender].ownedArt[i];
                counter++;
            }
        }
        userList[msg.sender].ownedArt = tempList;
        userList[_user].ownedArt.push(_artId);
        artToOwner[_artId] = _user;
        userList[msg.sender].requestsNum -= transferRequests[_artId].users.length;
        uint256 index;
        for (uint256 i = 0; i < transferRequests[_artId].users.length; i++) {
            if (transferRequests[_artId].users[i] == _user) {
                index = i;
            }
        }
        artToPrice[_artId] = transferRequests[_artId].proposedPrices[index];
        bool check = false;
        for (uint256 i = 0; i < artWithPrices.length; i++) {
            if (artWithPrices[i] == _artId) {
                check = true;
            }
        }
        if (check == false) {
            artWithPrices.push(_artId);
        }
        delete transferRequests[_artId];
        if (_exists(_artId)) {
            if (ownerOf(_artId) == msg.sender) {
                transferFrom(msg.sender, _user, _artId);
            }
        }
    }

    function requestArt(uint256 _artId, uint256 price) public notOwner(_artId) {
        require(checkRequest(msg.sender, _artId) == false, "You already requested this art");
        if (transferRequests[_artId].itemId != _artId) {
            address[] memory newAddrArray;
            uint256[] memory newPricesArray;
            Request memory newRequest = Request(_artId, newAddrArray, newPricesArray);
            transferRequests[_artId] = newRequest;
        }
        transferRequests[_artId].users.push(msg.sender);
        transferRequests[_artId].proposedPrices.push(price);
        userList[artToOwner[_artId]].requestsNum++;
    }

    function getRequests()
    public
    view
    returns (
        uint256[] memory,
        address[] memory,
        uint256[] memory
    )
    {
        uint256[] memory idArray = new uint256[](userList[msg.sender].requestsNum);
        address[] memory userArray = new address[](
            userList[msg.sender].requestsNum
        );
        uint256[] memory priceArray = new uint256[](
            userList[msg.sender].requestsNum
        );
        uint256[] storage userArt = userList[msg.sender].ownedArt;
        uint256 counter = 0;
        for (uint256 i = 0; i < userArt.length; i++) {
            if (transferRequests[userArt[i]].itemId != 0) {
                for (
                    uint256 j = 0;
                    j < transferRequests[userArt[i]].users.length;
                    j++
                ) {
                    idArray[counter] = transferRequests[userArt[i]].itemId;
                    userArray[counter] = transferRequests[userArt[i]].users[j];
                    priceArray[counter] = transferRequests[userArt[i]].proposedPrices[j];
                    counter++;
                }
            }
        }
        return (idArray, userArray, priceArray);
    }
}