/*
This software is distributed under MIT/X11 license

Copyright (c) 2021 Federico Sanna - University of Cagliari

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

import React, { useState, useEffect, Fragment, useRef } from "react";
import { pages } from "./PageEnum";

function NavBar(props) {
  const changeToOwnedArt = (e) => {
    e.preventDefault();
    props.setAppState(pages.OWNEDART);
  };

  const changeToHome = (e) => {
    e.preventDefault();
    props.setAppState(pages.HOME);
  };

  const changeToRequests = (e) => {
    e.preventDefault();
    props.setAppState(pages.REQUESTS);
  };

  const customClassActive = (btn) => {
    if (props.appState === btn) return "nav-item active";
    else return "nav-item";
  };

  useEffect(() => {
    if (
      props.appState === pages.ARTPAGE ||
      props.appState === pages.VALIDATEART ||
      props.appState === pages.CREATEPDF
    ) {
      document.getElementById("navhomebtn").className = "nav-item";
      document.getElementById("navartbtn").className = "nav-item";
      document.getElementById("navreqbtn").className = "nav-item";
    }
  }, [props.appState]);

  const checkNumRequests = () => {
    if (props.userData && props.userData.numRequests !== 0) {
      return "(" + props.userData.numRequests.toString() + ")";
    } else {
      return "";
    }
  };

  return (
    <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a className="navbar-brand" href="#" onClick={changeToHome}>
        Art Repository
      </a>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarsExampleDefault"
        aria-controls="navbarsExampleDefault"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>

      <div className="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul className="navbar-nav mr-auto">
          <li id="navhomebtn" className={customClassActive(pages.HOME)}>
            <a className="nav-link" href="#" onClick={changeToHome}>
              Home
            </a>
          </li>
          <li id="navartbtn" className={customClassActive(pages.OWNEDART)}>
            <a className="nav-link" href="#" onClick={changeToOwnedArt}>
              Le tue opere
            </a>
          </li>
          <li id="navreqbtn" className={customClassActive(pages.REQUESTS)}>
            <a className="nav-link" href="#" onClick={changeToRequests}>
              <b>Richieste {checkNumRequests()}</b>
            </a>
          </li>
        </ul>
      </div>
      <div
        id="navUserName"
        className="d-flex justify-content-end text-white mr-3"
      >
        Ciao, {props.userData.name}
      </div>
    </nav>
  );
}

export default NavBar;
