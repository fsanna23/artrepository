/*
This software is distributed under MIT/X11 license

Copyright (c) 2021 Federico Sanna - University of Cagliari

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

const fs = require("fs");
const PDFDocument = require("pdf-lib").PDFDocument;
const StandardFonts = require("pdf-lib").StandardFonts;
const fontkit = require("@pdf-lib/fontkit");
const Jimp = require("jimp");
const PDFDocumentWriter = require("pdf-lib").PDFWriter;
const utf8 = require("utf8");

function splitString(myString) {
  let splitted = myString.split(" ");
  if (splitted.length > 15) {
    let iter = 0;
    let newArray = [];
    while (iter < splitted.length) {
      const temp = splitted.slice(iter, iter + 15);
      newArray.push(temp.join(" "));
      iter += 15;
    }
    return newArray;
  }
  return [myString];
}

async function addImage(doc, img) {
  const newPage = doc.addPage();
  img = await doc.embedJpg(img);
  const { width, height } = img.scale(1);
  newPage.drawImage(img, {
    x: newPage.getWidth() / 2 - width / 2,
    y: newPage.getHeight() / 2 - height / 2,
  });
}

async function addSignature(doc, page, img, imgType, lastHeight) {
  if (imgType === "png") {
    img = await doc.embedPng(img);
  } else {
    img = await doc.embedJpg(img);
  }
  const { width, height } = img.scale(1);
  const centerWidth = page.getWidth() - (page.getWidth() - width);
  const centerHeight = page.getHeight() - (page.getHeight() - lastHeight / 2);
  page.drawImage(img, {
    x: centerWidth,
    y: centerHeight,
  });
}

async function manageArt(artJson, signature, authenticator, address, price) {
  const url = "https://pdf-lib.js.org/assets/ubuntu/Ubuntu-R.ttf";
  const fontBytes = await fetch(url).then((res) => res.arrayBuffer());
  const doc = await PDFDocument.create();
  doc.registerFontkit(fontkit);
  const myFont = await doc.embedFont(fontBytes);
  const myBoldFont = await doc.embedFont(StandardFonts.HelveticaBold);
  const page = doc.addPage();

  const textWidth = 40;
  let textHeight = 800;
  const parSize = 10;
  const titleSize = 14;

  page.moveTo(textWidth, textHeight);
  page.drawText("ID dell'opera", {
    font: myBoldFont,
    size: titleSize,
  });
  page.moveTo(textWidth + 100, textHeight);
  page.drawText(artJson.id, { font: myFont, size: parSize });
  textHeight -= 30;

  const title = splitString(artJson.title);
  page.moveTo(textWidth, textHeight);
  page.drawText("Titolo dell'opera", { font: myBoldFont, size: titleSize });
  textHeight -= 14;
  title.forEach((el) => {
    page.moveTo(textWidth, textHeight);
    page.drawText(el, { font: myFont, size: parSize });
    textHeight -= 12;
  });
  textHeight -= 30;

  try {
    const description = splitString(artJson.description);
    page.moveTo(textWidth, textHeight);
    page.drawText("Descrizione dell'opera", {
      font: myBoldFont,
      size: titleSize,
    });
    textHeight -= 14;
    description.forEach((el) => {
      page.moveTo(textWidth, textHeight);
      page.drawText(el, { font: myFont, size: parSize });
      textHeight -= 12;
    });
    textHeight -= 30;
  } catch (e) {
    console.log("Error in first part");
    console.log(e);
  }
  page.moveTo(textWidth, textHeight);
  page.drawText("Valore minimo stimato dell'opera", {
    font: myBoldFont,
    size: titleSize,
  });
  page.moveTo(textWidth + 300, textHeight);
  page.drawText(artJson.minEstimate, { font: myFont, size: parSize });
  textHeight -= 30;

  page.moveTo(textWidth, textHeight);
  page.drawText("Valore massimo stimato dell'opera", {
    font: myBoldFont,
    size: titleSize,
  });
  page.moveTo(textWidth + 300, textHeight);
  page.drawText(artJson.maxEstimate, { font: myFont, size: parSize });
  textHeight -= 30;

  page.moveTo(textWidth, textHeight);
  page.drawText("Prezzo di vendita dell'opera", {
    font: myBoldFont,
    size: titleSize,
  });
  page.moveTo(textWidth + 300, textHeight);
  page.drawText(price, { font: myFont, size: parSize });
  textHeight -= 50;

  page.moveTo(textWidth, textHeight);
  page.drawText("Nome dell'autenticatore", {
    font: myBoldFont,
    size: titleSize,
  });
  page.moveTo(textWidth + 300, textHeight);
  page.drawText(authenticator, { font: myFont, size: parSize });
  textHeight -= 30;

  page.moveTo(textWidth, textHeight);
  page.drawText("Address dell'autenticatore", {
    font: myBoldFont,
    size: titleSize,
  });
  page.moveTo(textWidth + 300, textHeight);
  page.drawText(address, { font: myFont, size: parSize });
  textHeight -= 100;

  let signExt = { jimp: null, ext: null };
  if (signature.name.endsWith("png")) {
    signExt.jimp = Jimp.MIME_PNG;
    signExt.ext = "png";
  } else {
    signExt.jimp = Jimp.MIME_JPEG;
    signExt.ext = "jpeg";
  }

  let newSignature = null;
  if (typeof signature === "string") {
    newSignature = signature.toString();
  } else {
    newSignature = fs.readFileSync(signature.path);
  }
  let signImage = null;

  try {
    signImage = await Jimp.read(newSignature);
  } catch (e) {
    throw e;
  }

  signImage.scale(0.2).quality(60);
  let bufferSignImage = await signImage.getBufferAsync(signExt.jimp);
  await addSignature(doc, page, bufferSignImage, signExt.ext, textHeight);

  for (let i = 0; i < artJson.numImages; i++) {
    let name = "./download/" + artJson.id;
    if (i !== 0) {
      name += "_" + (i + 1);
    }
    let image = await Jimp.read(name + ".jpg");
    image.scale(0.2);
    let bufferImage = await image.getBufferAsync(Jimp.MIME_JPEG);
    await addImage(doc, bufferImage);
  }
  return await doc.save();
}

module.exports.manageArt = manageArt;
