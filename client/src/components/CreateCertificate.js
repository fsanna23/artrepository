/*
This software is distributed under MIT/X11 license

Copyright (c) 2021 Federico Sanna - University of Cagliari

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

import React, { Fragment, useEffect, useReducer, useState } from "react";
import axios from "axios";
import { Button } from "react-bootstrap";
import { recGetImages } from "../utilities/serverCalls";

function CreateCertificate(props) {
  /*  The props contains the variables:
   *   - art, which is the art for which we want to create the PDF;
   *   - userData, data like username, address and the session
   *     (we are mostly going to use the session for this component)*/

  const [signature, setSignature] = useState(null);
  const [pdfState, setPdfState] = useState(0);
  const [axiosState, setAxiosState] = useState(false);
  const [link, setLink] = useState(null);
  const [error, setError] = useState("");

  const changeSignatureFile = (e) => {
    e.preventDefault();

    let file = e.target.files[0];
    let reader = new FileReader();

    if (e.target.files.length === 0) {
      return;
    }

    /*  The onloadend function is triggered after the readAsDataURL
        function.
        */

    console.log(URL.createObjectURL(file));
    reader.readAsDataURL(file);
    reader.onloadend = (e) => {
      setSignature({
        reader: reader.result,
        file: file,
      });
    };
  };

  // Displays an error and then removes it after 3 seconds
  const setTempError = (error) => {
    setError(error);
    setTimeout(function () {
      setError("");
    }, 5500);
  };

  useEffect(() => {
    (async function () {
      if (pdfState === 1) {
        setTimeout(async () => {
          let response = await axios.get("http://localhost:9000/getpdfstatus");
          if (
            response.data.pdfStatus &&
            response.data.pdfStatus === "uploaded"
          ) {
            setPdfState((state) => {
              return state + 1;
            });
            let linkResponse = await axios.get(
              "http://localhost:9000/getpdflink"
            );
            if (linkResponse.data.link) {
              setLink(linkResponse.data.link);
              let hashResponse = await axios.get(
                "http://localhost:9000/getpdfhash"
              );
              if (hashResponse.data.hash) {
                let price = props.art.price ? parseInt(props.art.price) : 0;
                await props.web3.contract.methods
                  .addToken(
                    // parseInt(props.art.id),
                    props.art.id,
                    props.art.title,
                    props.art.description,
                    // parseInt(props.art.minEstimate),
                    props.art.minEstimate,
                    // parseInt(props.art.maxEstimate),
                    props.art.maxEstimate,
                    // parseInt(props.art.price),
                    price,
                    props.userData.name,
                    props.userData.address,
                    "0x" + hashResponse.data.hash,
                    linkResponse.data.link
                  )
                  .send({ from: props.web3.accounts[0] });
                // Update app
                props.updateApp(null, null);
              }
            } else {
              console.log("Error getting pdf link from server");
            }
          } else {
            console.log("Status not right, it is:   ", response.data.pdfStatus);
          }
        }, 1);
      }
    })();
  }, [axiosState]);

  const createPdf = async (e) => {
    e.preventDefault();
    setPdfState((state) => {
      return state + 1;
    });
    const formData = new FormData();
    formData.append("signature", signature.file, signature.file.fileName);
    formData.append("id", props.art.id);
    formData.append("certAddress", props.userData.address);
    formData.append("certName", props.userData.name);
    formData.append("price", props.art.price ? props.art.price : 0);
    try {
      await axios.post("http://localhost:9000/createpdf", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          authorization: props.userData.token,
        },
      });
    } catch (e) {
      setTempError(
        "Errore DropBox! Ricarica il server per aggiornare la tua connessione!"
      );
      setPdfState(0);
    }
    console.log("Awaited and setting axios state to true");
    setAxiosState(true);
  };

  const checkPdfState = () => {
    const downloadPdf = async () => {
      window.open(
        "http://localhost:9000/getgeneratedpdf?id=" + props.art.id,
        "_blank"
      );
    };

    if (pdfState === 1) {
      return (
        <div className="container mt-3">
          <h6>Il PDF è in fase di creazione</h6>
        </div>
      );
    } else if (pdfState === 2) {
      return (
        <div className="container mt-3">
          <h6>Il PDF è stato caricato nel tuo DropBox</h6>
          <h3>
            Dropbox Link:{" "}
            <a target="_blank" href={link}>
              {link}
            </a>
          </h3>
          <Button className="mt-3" variant="primary" onClick={downloadPdf}>
            Scarica il PDF dal server
          </Button>
        </div>
      );
    } else return <Fragment />;
  };

  const checkArt = () => {
    if (props.art) {
      return (
        <div>
          <p className="text-center text-danger">{error}</p>
          <h3 className="h3">Stai per certificare: </h3>
          <h2>{props.art.title}</h2>
          {/*NOTE this part is for uploading the signature, we don't need it*/}
          <div className="form-group">
            <label className="h3 mt-5" htmlFor="inputSignature">
              Inserisci qua l'immagine della tua firma.
            </label>
            <input
              type="file"
              className="form-control-file"
              id="inputSignature"
              onChange={changeSignatureFile}
              name="signature"
            />
          </div>
          {signature !== null ? (
            <div className="container">
              <div>
                <img
                  src={signature.reader}
                  className="rounded w-25 h-25"
                  alt="signature"
                />
              </div>
              <div>
                <button className="btn btn-primary mt-2" onClick={createPdf}>
                  Crea PDF
                </button>
              </div>
            </div>
          ) : (
            <Fragment />
          )}
          {checkPdfState()}
        </div>
      );
    } else {
      return <h3 className="h3">Errore nella pagina creazione certificati!</h3>;
    }
  };

  return checkArt();
}

export default CreateCertificate;
