/*
This software is distributed under MIT/X11 license

Copyright (c) 2021 Federico Sanna - University of Cagliari

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

// Redirect URL to pass to Dropbox. Has to be whitelisted in Dropbox settings
const OAUTH_REDIRECT_URL = "http://localhost:9000/auth";

// Libraries used during authentication
const crypto = require("crypto"), // To create random state values for OAuth
  NodeCache = require("node-cache"), // To cache OAuth state parameter
  fs = require("fs"), // To read the file system
  // Dropbox libraries
  Dropbox = require("dropbox").Dropbox,
  fetch = require("isomorphic-fetch"),
  // PDF creator
  { PDFDocument } = require("pdf-lib"),
  { manageArt } = require("./pdfmanager"),
  formidable = require("formidable"),
  sha256File = require("sha256-file");
download = require("downloadjs");

const mycache = new NodeCache();

let uploadStatus = 0;

module.exports.home = async (req, res) => {
  let dbx = getDropboxInstance(req);

  if (!req.session.token) {
    console.log("No session token in home");
    authorize(dbx, req, res);
  } else {
    console.log("Session token in home found");
    res.cookie("token", req.session.token).send("Authenticated!");
  }
};

module.exports.auth = async (req, res) => {
  if (req.query.error_description) {
    console.log(req.query.error_description);
    res.status(500);
    return res.send("<h1>Error... yikes!</h1><p>Check your console!</p>");
  }

  // OAuth state value is only valid for 10 minutes
  // Session that created the state should be the same as the current session
  let state = req.query.state;
  let session_id = mycache.get(state);
  if (!session_id) {
    res.status(440);
    return res.send("<h1>Authentication timeout, please try again</p>");
  } else if (session_id != req.session.id) {
    res.status(500);
    console.log("Authorization flow was started under a different session");
    return res.send("<h1>Error... yikes!</h1><p>Check your console!</p>");
  }

  if (req.query.code) {
    let dbx = getDropboxInstance(req);

    try {
      let token = await dbx.getAccessTokenFromCode(
        OAUTH_REDIRECT_URL,
        req.query.code
      );

      // Store token and invalidate the state
      req.session.token = token;
      mycache.del(state);

      // Get the root_namespace for the user
      // Ensures that this flow works for Dropbox Business users team spaces
      // More info https://www.dropbox.com/developers/reference/namespace-guide
      dbx.setAccessToken(token);
      let account = await dbx.usersGetCurrentAccount();
      req.session.root_namespace_id = account.root_info.root_namespace_id;

      // Additionally save the user name to display it later
      req.session.name = account.name.given_name;

      res.redirect("/");
    } catch (error) {
      console.log(error);
      res.status(500);
      res.send("<h1>Error... yikes!</h1><p>Check your console!</p>");
    }
  }
};

module.exports.checkToken = async (req, res) => {
  console.log(req.cookies);
  if (req.cookies.token) {
    console.log("Token found in check token");
    res.status(200).json({ tokenStatus: "ok" });
  } else {
    console.log("Token NOT found in check token");
    res.status(200).json({ tokenStatus: "error" });
  }
};

module.exports.verifyHash = async (req, res) => {
  const savedHash = req.body.hash;
  const artId = req.body.id;
  console.log(req.body);
  if (!fs.existsSync(`./download/${artId}.pdf`)) {
    const link = req.body.link;
    await downloadFromLink(link, artId);
  }
  let pdfHash = "0x" + sha256File(`./download/${artId}.pdf`);
  console.log(savedHash);
  console.log(pdfHash);
  if (savedHash === pdfHash) {
    console.log("Correct hash");
    res.status(200).json({ status: "ok" });
  } else {
    console.log("Wrong hash");
    res.status(200).json({ status: "err" });
  }
};

module.exports.getArtJson = async (req, res) => {
  if (fs.existsSync("artData.json")) {
    fs.readFile("artData.json", (err, data) => {
      if (err) {
        res.status(500);
        throw err;
      }
      const myJson = JSON.parse(data.toString());
      if (myJson.length !== 0) {
        res.status(200).send(myJson);
        console.error("Json found AND not empty, okk");
      } else {
        res.status(200).json({ artStatus: "error" });
        console.error("Json found but empty");
      }
    });
  } else {
    res.status(200).json({ artStatus: "error" });
    console.error("No json found");
  }
};

// Sets art from Cambiaste
module.exports.setArtJson = async (req, res) => {
  const writeJson = (req, res) => {
    if (req.body.art.length !== 0) {
      try {
        fs.writeFileSync("artData.json", JSON.stringify(req.body.art));
        console.log("Successfully written new artData json");
        res.status(200).end();
      } catch (e) {
        console.log("Error writing new artData json");
        res.status(500).end();
      }
    } else {
      console.log("The array art in the request is empty");
      res.status(500).end();
    }
  };

  if (fs.existsSync("artData.json")) {
    fs.readFile("artData.json", (err, data) => {
      if (err) {
        res.status(500).end();
        throw err;
      }
      let myJson = JSON.parse(data.toString());
      if (myJson.length === 0) {
        console.log("WRITING NEW JSON");
        writeJson(req, res);
      } else {
        console.log("Found data in artJson, editing");
        myJson.map((el) => {
          for (const tempEl of req.body.art) {
            if (tempEl.id === el.id) {
              console.log("Editing object ", el.id);
              return Object.assign(el, tempEl);
            }
          }
        });
        fs.writeFileSync("artData.json", JSON.stringify(myJson));
        res.status(200).end();
      }
    });
  } else {
    // CREATE NEW JSON WITH DATA PASSED IN REQ
    console.log("WRITING NEW JSON");
    writeJson(req, res);
  }
};

module.exports.getImg = async (req, res) => {
  checkDownloadFolder();
  const numImages = req.query.numImages;
  let imgArray = [];
  for (let i = 0; i < numImages; i++) {
    let imageName = req.query.id;
    if (i !== 0) {
      imageName += `_${i + 1}`;
    }
    if (fs.existsSync("./download/" + imageName + ".jpg")) {
      imgArray.push(fs.readFileSync("./download/" + imageName + ".jpg"));
    } else {
      const folder = imageName[0];
      let link = `https://foto.cambiaste.com/Foto/Ridotte/${folder}/${req.query.id}/${imageName}.jpg`;
      const response = await fetch(link);
      const buffer = await response.buffer();
      fs.writeFileSync(`./download/${imageName}.jpg`, buffer);
      imgArray.push(fs.readFileSync("./download/" + imageName + ".jpg"));
    }
  }
  res.status(200).json({ status: "complete", files: imgArray });
};

/* OLD GET IMG
module.exports.getImg = async (req, res) => {
  if (fs.existsSync("./download/" + req.query.fileName)) {
    console.log("File exists!");
    fs.readFile("./download/" + req.query.fileName, (err, data) => {
      if (err) throw err;
      res.status(200).json({ status: "complete", file: data });
    });
  } else {
    //   //Old download with Dropbox
    // req.session.token = req.headers.authorization;
    // let dbx = getDropboxInstance(req);
    // downloadFile(dbx, req.query.fileName)
    //   .then((myFile) => {
    //     res.status(200).json({ status: "downloaded", file: myFile });
    //   })
    //   .catch((err) => {
    //     res.status(500).json({ status: "failed", reason: err });
    //   });
    //
    console.log("File does not exist!");
    res.status(400).json({ status: "error" });
  }
};

 */

module.exports.downloadImg = async (req, res) => {
  const response = await fetch(req.body.link);
  const buffer = await response.buffer();
  fs.writeFile(`./download/${req.body.name}.jpg`, buffer, () =>
    fs.readFile(`./download/${req.body.name}/jpg`, (err, data) => {
      if (err) throw err;
      res.status(200).json({ file: data });
    })
  );
};

module.exports.createPdf = async (req, res) => {
  const form = formidable({ multiples: true });

  form.parse(req, async (err, fields, files) => {
    if (err) return;
    req.session.token = req.headers.authorization;
    let dbx = getDropboxInstance(req);
    console.log(req.session.token);
    try {
      // Check list files
      let listResult = await getFiles(dbx);
      console.log(listResult);
    } catch (e) {
      console.log(e);
      res.status(500).json({ status: "error" });
      return;
    }
    //res.status(200).json({ status: "ok" });
    let artData = await getArtData(fields.id);
    mycache.set("pdfStatus", "creating");
    await checkImages(artData.id, artData.numImages);
    let signature = null;
    if (files.signature) {
      signature = files.signature;
    } else {
      signature = "./signature/signature.jpg";
    }
    let pdf = null;
    try {
      pdf = await manageArt(
        artData,
        signature,
        fields.certName,
        fields.certAddress,
        fields.price
      );
    } catch (e) {
      throw new Error("Error in managing art");
    }
    if (pdf !== null) {
      fs.writeFileSync(`./download/${fields.id}.pdf`, pdf);
      //Create hash
      let pdfHash = sha256File(`./download/${fields.id}.pdf`);
      mycache.set("pdfHash", pdfHash);
      mycache.set("pdfStatus", "uploading");
      let link;
      try {
        //Upload to DBox
        link = await uploadFile(dbx, pdf, `${fields.id}.pdf`);
      } catch (e) {
        console.log("Error in uploading");
        console.log(e);
      }
      if (link !== undefined) {
        mycache.set("pdfStatus", "uploaded");
        mycache.set("pdfLink", link.url);
        console.log("Finishing with create pdf and sending status");
        res.status(200).json({ status: "ok" });
      }
    }
  });
};

module.exports.getGeneratedPdf = async (req, res) => {
  const id = req.query.id;
  /*   The file doesn't exists only in case we're downloading a PDF
   *   which we haven't created (we haven't certified that art)*/
  if (!fs.existsSync(`./download/${id}.pdf`)) {
    const link = req.query.link;
    await downloadFromLink(link, id);
  }
  res.download(`./download/${id}.pdf`, `${id}.pdf`);
};

module.exports.getPdfLink = async (req, res) => {
  res.status(200).json({ link: mycache.get("pdfLink") });
};

module.exports.getPdfHash = async (req, res) => {
  res.status(200).json({ hash: mycache.get("pdfHash") });
};

module.exports.getPdfStatus = async (req, res) => {
  res.status(200).json({ pdfStatus: mycache.get("pdfStatus") });
};

// INTERNAL FUNCTIONS

// Kick starts the OAuth code exchange
function authorize(dbx, req, res, require_role) {
  // Create a random state value
  let state = crypto.randomBytes(16).toString("hex");
  // Save state and the session id for 10 mins
  mycache.set(state, req.session.id, 6000);
  // Get authentication URL and redirect
  authUrl = dbx.getAuthenticationUrl(OAUTH_REDIRECT_URL, state, "code");
  // Attach a require_role parameter if present
  if (req.query.require_role) {
    authUrl = authUrl + "&require_role=" + req.query.require_role;
  }

  res.redirect(authUrl);
}

// Gets a new instance of Dropbox for this user session
function getDropboxInstance(req) {
  let dbx_config = {
    fetch: fetch,
    clientId: process.env.DBX_APP_KEY,
    clientSecret: process.env.DBX_APP_SECRET,
  };

  let dbx = new Dropbox(dbx_config);

  if (req.session.token) {
    dbx.setAccessToken(req.session.token);
  }

  return dbx;
}

// Checks if the images for a certain art are found locally, else download them from DBOX
// NOTE: no need, the images are saved locally
async function checkImages(id, numImages) {
  checkDownloadFolder();
  for (let i = 0; i < numImages; i++) {
    let imageName = id;
    if (i !== 0) {
      imageName += `_${i + 1}`;
    }
    if (!fs.existsSync("./download/" + imageName + ".jpg")) {
      const folder = imageName[0];
      let link = `https://foto.cambiaste.com/Foto/Ridotte/${folder}/${id}/${imageName}.jpg`;
      const response = await fetch(link);
      const buffer = await response.buffer();
      console.log(buffer);
      fs.writeFileSync(`./download/${imageName}.jpg`, buffer);
    }
  }
}

// Gets the list of files in a certain folder in Dropbox
function getFiles(dbx) {
  const listFoldArg = {
    path: "",
    recursive: false,
    include_media_info: false,
    include_deleted: false,
    include_has_explicit_shared_members: false,
    include_mounted_folders: false,
  };

  return new Promise((resolve, reject) => {
    dbx
      .filesListFolder(listFoldArg)
      .then((result) => {
        //getFilesRecursive(result);
        resolve("ok");
      })
      .catch((err) => {
        //console.log(err);
        reject(err);
      });
  });
}

// Helper method for getFiles
function getFilesRecursive(results) {
  results.entries.map((result) => {
    console.log("Type: " + result[".tag"] + ", Name: " + result.name);
  });
  if (results.has_more) {
    console.log("\nHas more\n");
    dbx
      .filesListFolderContinue(results.cursor)
      .then((res) => getFilesRecursive(res))
      .catch((err) => console.log(err));
  }
}

// Boilerplate for creating a PDF file
async function generatePDF() {
  // Create a new document and add a new page
  const doc = await PDFDocument.create();
  const page = doc.addPage();

  // Load the image and store it as a Node.js buffer in memory
  let img = fs.readFileSync("./logo.png");
  img = await doc.embedPng(img);

  // Draw the image on the center of the page
  const { width, height } = img.scale(1);
  page.drawImage(img, {
    x: page.getWidth() / 2 - width / 2,
    y: page.getHeight() / 2 - height / 2,
  });

  // Write the PDF to a file
  fs.writeFileSync("./create.pdf", await doc.save());

  // Serialize the PDFDocument to bytes (a Uint8Array)
  const pdfBytes = await doc.save();
  return pdfBytes;
}

// Uploads a file to DropBox
function uploadFile(dbx, file, filename) {
  //let myObj = fs.readFileSync("./create.pdf");
  const filePath = `/${filename}`;
  const modeConfig = {
    ".tag": "overwrite",
  };
  const writeConfig = {
    contents: file, //uint8array
    path: filePath,
    mode: modeConfig,
    autorename: false,
    mute: true,
    strict_conflict: false,
  };
  const shareConfig = {
    path: filePath,
    short_url: false,
  };

  return new Promise((resolve, reject) => {
    dbx
      .filesUpload(writeConfig)
      .then((res) => {
        dbx
          .sharingCreateSharedLink(shareConfig)
          .then((res) => resolve(res))
          .catch((err) => reject(err));
      })
      .catch((err) => reject(err));
  });
}

// Downloads file from dropbox (writes the file on disk and returns the binary)
async function downloadFile(dbx, fileName) {
  const dwnConfig = { path: "/" + fileName };

  //console.log("Your path config for download", dwnConfig);

  return new Promise((resolve, reject) => {
    dbx
      .filesDownload(dwnConfig)
      .then((res) => {
        fs.writeFileSync("./download/" + fileName, res.fileBinary);
        console.log("Download done!");
        resolve(res.fileBinary);
      })
      .catch((err) => {
        console.log("Download error!");
        console.log(err);
        if (err.error_summary.includes("path/not_found/.")) {
          reject("Path not found");
        } else {
          reject(err.error_summary);
        }
      });
  });
}

// Download file from Dropbox shareable link
async function downloadFromLink(url, id) {
  if (!url.endsWith("=1")) {
    url = url.split("=")[0].concat("=1");
  }
  fetch(url)
    .then((response) => {
      if (response.status === 200) {
        const myFile = fs.createWriteStream(`./download/${id}.pdf`);
        response.body.pipe(myFile);
        response.body.on("error", console.log("Error writing stream"));
        myFile.on("finish", console.log("Finished writing"));
      }
    })
    .catch((err) => {
      console.log(err);
    });
}

// Gets the data id from the json file
function getArtData(id) {
  return new Promise((resolve, reject) => {
    fs.readFile("artData.json", (err, data) => {
      if (err) throw err;
      const myJson = JSON.parse(data.toString());
      myJson.forEach((el) => {
        if (el.id === id) {
          resolve(el);
        }
      });
      reject("File not found");
    });
  });
}

// Checks if the download folder exists, if not creates it
function checkDownloadFolder() {
  if (!fs.existsSync("./download/")) {
    fs.mkdirSync("./download/");
  }
}
