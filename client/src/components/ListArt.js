/*
This software is distributed under MIT/X11 license

Copyright (c) 2021 Federico Sanna - University of Cagliari

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

import React, { Fragment, useEffect, useState } from "react";
import { pages } from "./PageEnum";
import Modal from "react-bootstrap/Modal";
import FeedbackToast from "./FeedbackToast";
import axios from "axios";
import parseError from "../utilities/web3ErrorParser";

function ListArt(props) {
  const [showModal, setShowModal] = useState(false);
  const [showEtherscanModal, setShowEtherscanModal] = useState(false);
  const [ethScanID, setEthScanID] = useState(undefined);
  const [chainName, setChainName] = useState("");
  const [value, setValue] = useState("");
  const [selected, setSelected] = useState(null);
  const [inputError, setInputError] = useState("");

  // Modal functions
  const handleClose = () => setShowModal(false);
  const handleShow = () => {
    setValue("");
    setInputError("");
    setShowModal(true);
  };

  // Etherscan Modal Functions
  const handleCloseEthMod = () => {
    setShowEtherscanModal(false);
    setEthScanID(undefined);
  };
  const handleShowEthMod = (id) => {
    setShowEtherscanModal(true);
    setEthScanID(id);
  };

  // Etherscan Modal Functions
  const checkChainID = () => {
    return props.web3.web3.eth.getChainId().then((chainID) => {
      console.log("Chain ID is: ", chainID);
      switch (chainID) {
        case 3:
          return "ropsten";
        case 4:
          return "rinkeby";
        case 5:
          return "goerli";
        case 42:
          return "kovan";
        default:
          return "";
      }
    });
  };

  // Set chain ID
  useEffect(() => {
    checkChainID().then((chainName) => setChainName(chainName));
    // console.log("Chain name is ", chainName);
  }, []);

  // Calculates link for Etherscan
  const createEtherscanLink = () => {
    const contractAddress = props.web3.contract.options.address;
    const link = `https://${chainName}.etherscan.io/address/${contractAddress}#code`;
    return (
      <a target="_blank" href={link}>
        {link}
      </a>
    );
  };

  const sendRequest = async (price) => {
    console.log("Your price is: ", price);
    if (parseInt(price) < 1) {
      setInputError("Hai inserito un valore non corretto");
      return;
    }
    handleClose();
    try {
      console.log("Accounts:", props.web3.accounts);
      await props.web3.contract.methods
        .requestArt(selected.id, price)
        .send({ from: props.web3.accounts[0] });
    } catch (e) {
      let error = parseError(e.message);
      console.log(error);
      if (error === "You already requested this art") {
        props.openToast("Hai già richiesto questa opera!", "err");
      }
      if (error === "Hai annullato la richiesta!") {
        props.openToast("Hai annullato la richiesta!", "err");
      }
    }
  };

  const enterProduct = (e, prod) => {
    e.preventDefault();
    props.setAppState(pages.ARTPAGE);
    let type;
    if (props.certifiedArt) {
      type = "certified";
    } else if (props.userArt) {
      type = "owned";
    } else if (props.noArt) {
      type = "notowned";
    }
    props.setViewArt({ art: prod, type: type });
  };

  const enterCertificate = (e, prod) => {
    e.preventDefault();
    props.setAppState(pages.CREATEPDF);
    let type;
    if (props.certifiedArt) {
      type = "certified";
    } else if (props.userArt) {
      type = "owned";
    } else if (props.noArt) {
      type = "notowned";
    }
    props.setViewArt({ art: prod, type: type });
  };

  const enterVerify = (e, prod) => {
    e.preventDefault();
    (async function () {
      let response = await props.web3.contract.methods
        .getCertifiedArtData(prod.id)
        .call({ from: props.web3.accounts[0] });
      const pdfLink = response[2];
      const pdfHash = response[3];
      axios
        .post("http://localhost:9000/verifyhash", {
          id: prod.id,
          link: pdfLink,
          hash: pdfHash,
        })
        .then((response) => {
          // Check if status === "ok"
          if (response.data.status === "ok") {
            props.openToast("Il file PDF ha un hash corretto!", "ok");
          } else {
            props.openToast("Errore! Gli hash non corrispondono!", "err");
          }
        })
        .catch((err) => {
          console.log(err);
        });
    })();
  };

  const onRequestArt = (e, prod) => {
    e.preventDefault();
    setSelected(prod);
    handleShow();
  };

  const checkCertifiedArt = () => {
    const renderCertified = () => {
      return (
        <div>
          <h3 className="h3 mb-3">Le tue opere certificate</h3>
          {props.certifiedArt.length !== 0 ? (
            props.certifiedArt.map((el) => {
              return (
                <div className="card text-left" key={el.id}>
                  <h5 className="card-header">{el.title}</h5>
                  <span className="card-body">
                    <button
                      className="btn btn-primary"
                      onClick={(e) => {
                        enterProduct(e, el);
                      }}
                    >
                      Mostra opera
                    </button>
                    <button
                      className="btn btn-success ml-2"
                      onClick={(e) => {
                        enterVerify(e, el);
                      }}
                    >
                      Verifica autenticità
                    </button>
                    <button
                      className="btn btn-success ml-3"
                      onClick={() => {
                        handleShowEthMod(el.id);
                      }}
                    >
                      Mostra su Etherscan
                    </button>
                  </span>
                </div>
              );
            })
          ) : (
            <p>Non hai nessuna opera certificata</p>
          )}
        </div>
      );
    };
    const renderOwned = () => {
      return (
        <div>
          <h3 className="h3 mb-3 mt-3">Le tue opere non certificate</h3>
          {props.userArt.length !== 0 ? (
            props.userArt.map((el) => {
              return (
                <div className="card text-left" key={el.id}>
                  <h5 className="card-header">{el.title}</h5>
                  <span className="card-body">
                    <button
                      className="btn btn-primary"
                      onClick={(e) => {
                        enterProduct(e, el);
                      }}
                    >
                      Mostra opera
                    </button>
                    <button
                      className="btn btn-danger ml-2"
                      onClick={(e) => {
                        enterCertificate(e, el);
                      }}
                    >
                      Certifica opera
                    </button>
                  </span>
                </div>
              );
            })
          ) : (
            <p>Non possiedi nessuna opera</p>
          )}
        </div>
      );
    };
    const renderNotOwned = () => {
      return (
        <div>
          <h3 className="h3 mb-3">Sfoglia le opere che puoi acquistare</h3>
          {props.noArt.length !== 0 ? (
            props.noArt.map((el) => {
              return (
                <div className="card text-left" key={el.id}>
                  <h5 className="card-header">{el.title}</h5>
                  <span className="card-body">
                    <button
                      className="btn btn-primary"
                      onClick={(e) => {
                        enterProduct(e, el);
                      }}
                    >
                      Mostra opera
                    </button>
                    <button
                      className="btn btn-success ml-2"
                      onClick={(e) => {
                        onRequestArt(e, el);
                      }}
                    >
                      Richiedi opera
                    </button>
                  </span>
                </div>
              );
            })
          ) : (
            <p>Non vi sono opere</p>
          )}
        </div>
      );
    };

    if (props.certifiedArt) {
      return renderCertified();
    } else if (props.userArt) {
      return renderOwned();
    } else if (props.noArt) {
      return renderNotOwned();
    } else return <Fragment />;
  };

  return (
    <div>
      {checkCertifiedArt()}
      <Modal show={showModal} onHide={handleClose} size="lg">
        <Modal.Header>
          <Modal.Title>
            Stai richiedendo l'opera all'attuale possessore
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>{selected !== null ? selected.title : ""}</p>
          <p>Inserisci il tuo prezzo in Euro</p>
          <input
            type="number"
            value={value}
            min="1"
            onChange={(e) => {
              setValue(e.target.value);
            }}
          />
          <p className="text-danger">{inputError}</p>
        </Modal.Body>
        <Modal.Footer>
          <button
            className="btn btn-danger"
            onClick={() => {
              handleClose();
            }}
          >
            Chiudi
          </button>
          <button
            className="btn btn-primary"
            onClick={async () => {
              await sendRequest(value);
            }}
          >
            Invia richiesta
          </button>
        </Modal.Footer>
      </Modal>
      <Modal show={showEtherscanModal} onHide={handleCloseEthMod} size="lg">
        <Modal.Header>
          <Modal.Title>Visualizza il contratto su Etherscan</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Per visualizzare il contratto contenente quest'opera d'arte su
            Etherscan entra nel seguente link:
          </p>
          {createEtherscanLink()}
          <p>
            Se preferisci, puoi verificare l'autenticità dell'opera manualmente!
            Entra nel contratto su Etherscan, vai nella sezione{" "}
            <i>Read Contract </i> e assicurati di aver connesso il tuo wallet al
            sito di Etherscan. Dopodichè cerca la funzione denominata{" "}
            <i>getHash </i> e inserisci nel campo ID il seguente identificatore:
          </p>
          <h5>{ethScanID}</h5>
          <p>
            Ti verrà restituito l'hash del PDF caricato sulla Blockchain. Potrai
            usarlo per verificarne l'uguaglianza con l'hash del PDF presente su
            DropBox!
          </p>
        </Modal.Body>
        <Modal.Footer>
          <button
            className="btn btn-danger"
            onClick={() => {
              handleCloseEthMod();
            }}
          >
            Chiudi
          </button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default ListArt;
