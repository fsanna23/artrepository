/*
This software is distributed under MIT/X11 license

Copyright (c) 2021 Federico Sanna - University of Cagliari

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

import ListArt from "./ListArt";
import React, { Fragment, useState } from "react";
import FeedbackToast from "./FeedbackToast";

function OwnedArtPage(props) {
  // Toast states
  const [hashToast, setHashToast] = useState(false);
  const [toastMessage, setToastMessage] = useState({
    msg: "",
    type: "",
  });

  // Toast functions
  const toggleShowToast = () => {
    setHashToast(!hashToast);
    setToastMessage({ msg: "", type: "" });
  };

  const onOpenToast = (msg, type) => {
    setToastMessage({
      msg,
      type,
    });
    setHashToast(true);
  };

  return (
    <Fragment>
      <ListArt
        certifiedArt={props.certifiedArt}
        setAppState={props.setAppState}
        setViewArt={props.setViewArt}
        openToast={onOpenToast}
        web3={props.web3}
      />
      <ListArt
        userArt={props.art}
        setAppState={props.setAppState}
        setViewArt={props.setViewArt}
        web3={props.web3}
      />
      <FeedbackToast
        message={toastMessage}
        show={hashToast}
        toggleShow={toggleShowToast}
      />
    </Fragment>
  );
}

export default OwnedArtPage;
