/*
This software is distributed under MIT/X11 license

Copyright (c) 2021 Federico Sanna - University of Cagliari

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

import axios from "axios";
import $ from "jquery";

export const getProductsLinks = async () => {
  const defaultSite =
    "https://thingproxy.freeboard.io/fetch/https://cambiaste.com/it/asta-0500/scultura-e-oggetti-darte.asp?action=reset";
  const scrollSite =
    "https://thingproxy.freeboard.io/fetch/https://www.cambiaste.com/include/inc-basket-lots-list-infinite-view.asp";
  let headerOptions = {
    headers: {
      accept: "*/*",
      "accept-language": "it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7",
      "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
      "sec-fetch-dest": "empty",
      "sec-fetch-mode": "cors",
      "sec-fetch-site": "same-origin",
      "x-requested-with": "XMLHttpRequest",
      cookie:
        "ASPSESSIONIDQUQTTBRD=CNEPJBGAPBMJFOIHKKKMNGLH; _ga=GA1.2.1773847378.1599658386; _gid=GA1.2.751679072.1599658386; _fbp=fb.1.1599658386682.238036246; _jsuid=3686482398; cookiesDirective=1; registrazione%5FCambi+Casa+d%27Aste=attivato=1&pagineVis=3; _first_pageview=1; _gat_UA-23082461-1=1",
    },
    referrer:
      "https://www.cambiaste.com/it/asta-0500/scultura-e-oggetti-darte.asp?",
    referrerPolicy: "no-referrer-when-downgrade",
    body:
      "ajx=1&idAsta=582&sesN=asta-0500&cat=&cat2=&aut=&amt=&ordB=lot&htmlI=&tipA=False&ven=True&inVen=False&usr=&arc=True&charStr=iso-8859-1&limit=24&offset=24&lan=it&deb=",
    method: "POST",
    mode: "cors",
  };

  /*  Contains an object for each art taken from the website.
   *   For each art, the attributes will be:
   *   - The ID;
   *   - The link to follow to get the right info.
   * */
  let artArray = [];

  let findAndPush = async (response, artArray) => {
    const iconv = require("iconv-lite");
    let body = await response.text();
    body =
      '<div id="body-mock">' +
      body.replace(/^[\s\S]*<body.*?>|<\/body>[\s\S]*$/gi, "") +
      "</div>";
    // Take the items
    let items = $(body).find(".lotItemList");
    // Take the ids
    let ids = items
      .map(function () {
        return $(this).attr("id");
      })
      .get();
    // Take the links
    let links = items.get().map((item) => {
      return item.attributes[3].value.split("href='")[1].split("'")[0];
    });
    // Inserting in the array the object containing both values
    for (let i = 0; i < items.length; i++) {
      artArray.push({
        id: ids[i],
        link: links[i],
      });
    }
  };

  let response = null;

  // First batch
  try {
    response = await fetch(defaultSite, { method: "GET" });
    await findAndPush(response, artArray);
    console.log("Finished first batch");
  } catch (e) {
    console.log(e);
    console.log(response);
  }

  for (let i = 0; i < 3; i++) {
    if (i === 1) {
      headerOptions.headers.cookie =
        "ASPSESSIONIDQUQTTBRD=CNEPJBGAPBMJFOIHKKKMNGLH; _ga=GA1.2.1773847378.1599658386; _gid=GA1.2.751679072.1599658386; _fbp=fb.1.1599658386682.238036246; _jsuid=3686482398; cookiesDirective=1; registrazione%5FCambi+Casa+d%27Aste=attivato=1&pagineVis=3";
      headerOptions.body =
        "ajx=1&idAsta=582&sesN=asta-0500&cat=&cat2=&aut=&amt=&ordB=lot&htmlI=&tipA=False&ven=True&inVen=False&usr=&arc=True&charStr=iso-8859-1&limit=24&offset=48&lan=it&deb=";
    } else if (i === 2) {
      headerOptions.headers.cookie =
        "ASPSESSIONIDQUQTTBRD=CNEPJBGAPBMJFOIHKKKMNGLH; _ga=GA1.2.1773847378.1599658386; _gid=GA1.2.751679072.1599658386; _fbp=fb.1.1599658386682.238036246; _jsuid=3686482398; cookiesDirective=1; registrazione%5FCambi+Casa+d%27Aste=attivato=1&pagineVis=3";
      headerOptions.body =
        "ajx=1&idAsta=582&sesN=asta-0500&cat=&cat2=&aut=&amt=&ordB=lot&htmlI=&tipA=False&ven=True&inVen=False&usr=&arc=True&charStr=iso-8859-1&limit=24&offset=72&lan=it&deb=";
    }
    try {
      response = await fetch(scrollSite, headerOptions);
      await findAndPush(response, artArray);
      console.log("Finished " + i + " batch");
    } catch (e) {
      console.log(e);
      console.log(response);
    }
  }

  return artArray;
};

export const getProductInfo = async (artJson) => {
  const iconv = require("iconv-lite");
  let headerOptions = {
    method: "GET",
    headers: {
      accept: "*/*",
      "content-type": "text/html; charset=ISO-8859-1",
    },
  };
  let response = null;
  try {
    response = await fetch(
      "https://thingproxy.freeboard.io/fetch/" + artJson.link,
      headerOptions
    );
    let body = await response.arrayBuffer();
    body = iconv.decode(new Buffer(body), "ISO-8859-1").toString();
    body =
      '<div id="body-mock">' +
      body.replace(/^[\s\S]*<body.*?>|<\/body>[\s\S]*$/gi, "") +
      "</div>";

    let [title, description] = $(body)
      .find("#descLotto")[0]
      .innerHTML.split("<!---->");

    artJson.title = title.replace(/<br>/g, " ").trim();
    artJson.description = description.replace(/<br>/g, " ").trim();

    artJson.numImages = $(body).find(".tmpSlide").get().length;

    let estimate = $(body)
      .find("#LottoVal h4")[0]
      .innerHTML.replace(/^[\s\S]*EUR/gi, "")
      .split("-");
    let min = estimate[0].trim();
    let max = estimate[1].trim();
    if (min.includes(",")) {
      artJson.minEstimate = min.substring(0, min.length - 3).replace(".", "");
    } else {
      artJson.minEstimate = min.replace(".", "");
    }
    if (max.includes(",")) {
      artJson.maxEstimate = max.substring(0, max.length - 3).replace(".", "");
    } else {
      artJson.maxEstimate = max.replace(".", "");
    }
  } catch (e) {
    console.log(e);
    console.log(response);
  }
};

export const getProductImages = async (id, numImages) => {
  const folder = id[0];
  let link = `https://foto.cambiaste.com/Foto/Ridotte/${folder}/${id}/${id}`;
  let imgArray = [];
  let imageName = id;
  for (let i = 0; i < numImages; i++) {
    if (i !== 0) {
      link += `_${i + 1}`;
      imageName += `_${i + 1}`;
    }
    link += ".jpg";
    axios
      .post("http://localhost:9000/downloadimg", {
        link: link,
        name: imageName,
      })
      .then((res) => {
        imgArray.push(res.data.file);
      })
      .catch((err) => {
        throw err;
      });
  }
  return imgArray;
};
