/*
This software is distributed under MIT/X11 license

Copyright (c) 2021 Federico Sanna - University of Cagliari

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

import React, { useState, Fragment, useEffect } from "react";
import { Toast } from "react-bootstrap";

function FeedbackToast(props) {
  useEffect(() => {
    console.log(props.message);
  }, [props.show]);

  const checkType = () => {
    if (props.message.type === "ok") {
      return "text-success";
    } else if (props.message.type === "err") {
      return "text-danger";
    } else return "";
  };

  const checkHeader = () => {
    if (props.message.msg.includes("hash")) {
      return "Verifica dell'hash";
    } else {
      return "Informazione";
    }
  };

  return (
    <div>
      <Toast
        style={{
          position: "fixed",
          left: "42%",
          top: "42%",
        }}
        show={props.show}
        onClose={props.toggleShow}
        delay={3000}
        autohide
      >
        <Toast.Header>
          <img src="holder.js/20x20?text=%20" className="rounded mr-2" alt="" />
          <strong className="mr-auto">{checkHeader()}</strong>
        </Toast.Header>
        <Toast.Body className={checkType()}>
          <h5>{props.message.msg}</h5>
        </Toast.Body>
      </Toast>
    </div>
  );
}

export default FeedbackToast;
