/*
This software is distributed under MIT/X11 license

Copyright (c) 2021 Federico Sanna - University of Cagliari

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

import React, { useState, useEffect, Fragment, useRef } from "react";
import axios from "axios";
import "./App.css";
import NavBar from "./components/Navbar";
import Main from "./components/Main";
import Product from "./components/Product";
import CreateCertificate from "./components/CreateCertificate";
import FeedbackToast from "./components/FeedbackToast";
import OwnedArtPage from "./components/OwnedArtPage";
import Login from "./components/Login";
import { pages } from "./components/PageEnum";
import Requests from "./components/Requests";
// ArtInfo
import { getProductsLinks, getProductInfo } from "./utilities/getArtInfo";
// Web3
import getWeb3 from "./utilities/getWeb3";
import ArtCertificate from "./contracts/ArtCertificate.json";
import Web3 from "web3";

const MAX_CHUNK_REQUEST = 10;

const DEFAULT_ADDRESSES_LAPTOP = [
  {
    address: "0x62ceed6a2e86f1a4ba54cc7e9c1c1d61e229109a",
    name: "Mario Rossi",
  },
  {
    address: "0xafdb5c9612c0961bb7f577202f5888d009ca84b0",
    name: "Tommaso Bianchi",
  },
  { address: "0xdf386776d417eca14124c592bb05e3ad0a747c94", name: "Marco Neri" },
  {
    address: "0x9cdcd45600d0b883a53dd5c8d3769e2429cdeebb",
    name: "Rosa Antonelli",
  },
  {
    address: "0x5d9f40b2edb64fcf790158974e737d02d2956bb2",
    name: "Teresa Paoli",
  },
  {
    address: "0x8afb16ae479012ec6b40834c6c1c37bb5786a6e7",
    name: "Luca Sacchi",
  },
];

const DEFAULT_ADDRESSES_DESKTOP = [
  {
    address: "0x78EBc0b5Cb7ffC07d5eED2BFF51831D351668eCa",
    name: "Mario Rossi",
  },
  {
    address: "0x2CACB4da4C91f915c8974df749bC4c31c33039A8",
    name: "Tommaso Bianchi",
  },
  { address: "0x443c72847a8C11c2a921eE6f53a13992ca2b7151", name: "Marco Neri" },
  {
    address: "0x07D3F5Ad7991447AC1716BCAf95609601F3c30Ec",
    name: "Rosa Antonelli",
  },
  {
    address: "0x1dF59AC5591e68b4294A6Dc2D94fF8Bcc0201F6E",
    name: "Teresa Paoli",
  },
  {
    address: "0xce1d128fBce3684e2F854a293A9a56B3C67A8F15",
    name: "Luca Sacchi",
  },
];

const GOERLI_ADDRESSES = [
  {
    address: "0xa4EB0F6eD221723E019aAc08e08aA3a2cf24DCdd",
    name: "Mario Rossi",
  },
  {
    address: "0x4a2D3C0CdBaE17d9D485C98d93e3a9F35EE8638D",
    name: "Tommaso Bianchi",
  },
];

function App() {
  const [art, setArt] = useState([]); //list of not certified art
  const [certifiedArt, setCertifiedArt] = useState([]); //list of certified art
  const [noArt, setNoArt] = useState([]); // list of art not owned by user
  const [appState, setAppState] = useState(pages.HOME);
  const [viewArt, setViewArt] = useState({}); //view the product and print pdf
  const [userData, setUserData] = useState({}); //logged user data
  const [web3State, setWeb3State] = useState({});
  const [artState, setArtState] = useState({
    stateFlag: 0,
    currentNum: 0,
    totalNum: 0,
  }); // Info on art loading
  const [error, setError] = useState("");

  // Displays an error and then removes it after 3 seconds
  const setTempError = (error) => {
    setError(error);
    setTimeout(function () {
      setError("");
    }, 5500);
  };

  const checkOwnedArt = (artJson, ownedArt, certifArt) => {
    let arrayOwnedArt = artJson.filter((art) =>
      ownedArt.some((el) => el === art.id)
    );
    let arrayNotOwnedArt = artJson.filter((art) =>
      ownedArt.every((el) => el !== art.id)
    );
    let arrayNotCertArt = arrayOwnedArt.filter((art) =>
      certifArt.every((el) => el !== art.id)
    );
    let arrayCertArt = arrayOwnedArt.filter((art) =>
      certifArt.some((el) => el === art.id)
    );
    setArt(arrayNotCertArt);
    setCertifiedArt(arrayCertArt);
    setNoArt(arrayNotOwnedArt);
    return [arrayNotCertArt, arrayCertArt, arrayNotOwnedArt];
  };

  const setNewArtState = (cnum, tnum) => {
    setArtState({ totalNum: tnum, stateFlag: 3, currentNum: cnum });
  };

  const setRequests = (requests) => {
    setUserData({ ...userData, requests: requests });
  };

  const getArtInfo = async (artJson, flag) => {
    const myTimeOut = (ms) => {
      return new Promise((resolve) => setTimeout(resolve, ms));
    };
    let i;
    let tempJson = []; //Array that has to be sent to server (max 10 items)
    let tempArtJson = artJson; //Json that is going to replace the state
    for (i = 0; i < artJson.length; i++) {
      setNewArtState(i + 1, artJson.length);
      if (!artJson[i].title) {
        let tempArt = { ...artJson[i] };
        await getProductInfo(tempArt);
        tempJson.push(tempArt);
        tempArtJson[i] = Object.assign(artJson[i], tempArt);
        switch (flag) {
          case "ownedart":
            setArt(tempArtJson);
            break;
          case "certifiedart":
            setCertifiedArt(tempArtJson);
            break;
          case "noart":
            setNoArt(tempArtJson);
            break;
          default:
            break;
        }
        if (
          tempJson.length !== 1 &&
          tempJson.length % MAX_CHUNK_REQUEST === 1
        ) {
          // Upload to server
          await axios.post("http://localhost:9000/setartjson", {
            art: tempJson,
          });
          tempJson = [];
        }
        await myTimeOut(2000);
      }
    }
    if (tempJson.length % MAX_CHUNK_REQUEST !== 1) {
      axios
        .post("http://localhost:9000/setartjson", {
          art: tempJson,
        })
        .then((res) => {})
        .catch((err) => {
          console.log(err);
          setTempError("Errore nell'inserimento nuovi dati nel server");
        });
    }
  };

  const getJsonArt = async (artJson, tempArt, tempCertArt) => {
    const finalize = (arrayResults) => {
      return new Promise(async (resolve) => {
        // First I get the art info for art not owned
        await getArtInfo(arrayResults[2], "noart");
        // Then for the owned art
        await getArtInfo(arrayResults[0], "ownedart");
        // Finally for the certified art
        await getArtInfo(arrayResults[1], "certifiedart");
        resolve(true);
      });
    };

    try {
      // Assert that tempArt and tempCertArt are already full
      let arrayResults = await checkOwnedArt(artJson, tempArt, tempCertArt);
      setArtState({
        ...artState,
        stateFlag: 3,
        totalNum: arrayResults[2].length,
      });
      finalize(arrayResults)
        .then(() => {
          setArtState({ ...artState, stateFlag: 0 });
        })
        .catch((err) => {
          console.log(err);
          setTempError("Errore nell'impostazione dello stato dell'app");
        });
    } catch (e) {
      throw e;
    }
  };

  const randomizeArtOwners = (artJson) => {
    let initialOwnerAddress = [];
    let initialOwnerName = [];
    let initialArt = [];
    for (let i = 0; i < artJson.length; i++) {
      const rndNum = Math.floor(Math.random() * GOERLI_ADDRESSES.length);
      initialOwnerAddress.push(GOERLI_ADDRESSES[rndNum].address);
      initialOwnerName.push(GOERLI_ADDRESSES[rndNum].name);
      initialArt.push(artJson[i].id);
    }
    return [initialOwnerAddress, initialOwnerName, initialArt];
  };

  const setWeb3 = async () => {
    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();
      web3.eth.defaultAccount = accounts[0];

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = ArtCertificate.networks[networkId];
      const instance = new web3.eth.Contract(
        ArtCertificate.abi,
        deployedNetwork && deployedNetwork.address
      );

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      setWeb3State({ ...web3State, web3, accounts, contract: instance });
      return [instance, accounts];
    } catch (error) {
      // Catch any errors for any of the above operations.

      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`
      );
      console.error(error);
    }
  };

  // Gets JSON art from the server or from CambiAste if not found
  const getFromServerOrSite = async () => {
    setArtState({ ...artState, stateFlag: 1 });
    let artJson = null;
    let response = await axios.get("http://localhost:9000/getartjson");
    if (response.data.artStatus === "error") {
      // Nessun JSON trovato, devo crearlo
      setArtState({ ...artState, stateFlag: 2 });
      artJson = await getProductsLinks();
      // Sets only the ids on the server
      let setJsonResponse = await axios.post(
        "http://localhost:9000/setartjson",
        {
          art: artJson,
        }
      );
      setArtState({ ...artState, stateFlag: 4 });
      if (setJsonResponse.status !== 200) {
        console.error("Error in setting the art Json");
      }
    } else {
      // Ho trovato un JSON nel server
      artJson = response.data;
    }
    return artJson;
  };

  // Called when a request is accepted or when an art is certified
  const updateApp = async (newWeb3, redirect) => {
    //  Redirect to homepage
    if (redirect) setAppState(pages.HOME);
    let userArt;
    //  Call getUserArt from solidity
    if (newWeb3) {
      // Update current user
      const newAccount = newWeb3.eth.defaultAccount;
      const checkUser = await web3State.contract.methods
        .checkExistingUser(newAccount)
        .call();

      if (checkUser === true) {
        const userData = await web3State.contract.methods
          .getUserData(newAccount)
          .call({ from: newAccount });

        setUserData({
          name: userData[0],
          address: newAccount,
          numRequests: userData[1],
          token: document.cookie
            .split("; ")
            .find((row) => row.startsWith("token"))
            .split("=")[1],
        });

        userArt = await web3State.contract.methods
          .getUserArt()
          .call({ from: newWeb3.eth.defaultAccount });
      } else {
        setUserData({});
        return;
      }
    } else {
      console.log("ENTERED NOW WEBWE3");
      const userData = await web3State.contract.methods
        .getUserData(web3State.web3.eth.defaultAccount)
        .call();

      setUserData({
        ...userData,
        numRequests: userData[1],
      });
      userArt = await web3State.contract.methods
        .getUserArt()
        .call({ from: web3State.web3.eth.defaultAccount });
    }

    let artJson = await getFromServerOrSite();

    //  Update ownedArt, certifiedArt and notOwnedArt
    const tempArt = userArt[0];
    let tempCertArt = userArt[1];
    const soldArt = userArt[2];
    const prices = userArt[3];
    if (tempCertArt[0] === "0" || tempCertArt[0] === 0) {
      tempCertArt = [];
    }

    // Set prices
    for (let i = 0; i < soldArt.length; i++) {
      artJson.map((el) => {
        if (el.id === soldArt[i]) {
          let tempEl = el;
          tempEl.price = prices[i];
          return tempEl;
        }
      });
    }

    checkOwnedArt(artJson, tempArt, tempCertArt);
    setArtState({ ...artState, stateFlag: 0 });

    //  Update requests is done on the Request component
  };

  // DEFAULT useEffect
  useEffect(() => {
    if (document.cookie === "") {
      setTempError("Devi prima autenticarti su Dropbox dal server!");
      return;
    }

    (async function () {
      // Check for server and token availability
      /*try {
        await axios.get("http://localhost:9000/checktoken");
      } catch (e) {
        if (e.toString().includes("Network Error")) {
          setTempError("Il server non risponde!");
          return;
        }
      }*/
      const [contract, accounts] = await setWeb3();
      if (contract) {
        console.log("The contract is", contract);
        let artJson = await getFromServerOrSite();
        /*    Prima chiamo checkUser per capire se sono stati già aggiunti degli address
         *    nella lista utenti del contratto. La funzione fillUser viene utilizzata
         *    al posto del costruttore per inserire una prima lista di possessori
         *    delle opere d'arte, e viene quindi eseguita una sola volta.
         *    Viene utilizzato un valore booleano isConstructed nel contratto per capire
         *    se c'è già stato o meno il filling.
         */
        let isConstructed;
        try {
          isConstructed = await contract.methods.checkConstructed().call();
        } catch (e) {
          setTempError("Impossibile richiamare il contratto!");
          return;
        }
        if (isConstructed === false) {
          console.log("Not yet constructed. Filling now.");
          // Randomize art
          const [
            initialOwnerAddress,
            initialOwnerName,
            initialArt,
          ] = randomizeArtOwners(artJson);

          // Fills the contract with the first values
          await contract.methods
            .fillUser(initialOwnerAddress, initialOwnerName, initialArt)
            .send({ from: accounts[0] });
        }

        try {
          // Check if user exists
          const checkUser = await contract.methods
            .checkExistingUser(accounts[0])
            .call();

          //console.log("The checkuser is:", checkUser);

          if (checkUser === true) {
            console.log("Check user true");
            const userData = await contract.methods
              .getUserData(accounts[0])
              .call();

            setUserData({
              name: userData[0],
              address: accounts[0],
              numRequests: userData[1],
              token: document.cookie
                .split("; ")
                .find((row) => row.startsWith("token"))
                .split("=")[1],
            });

            const userArt = await contract.methods.getUserArt().call();

            console.log(userArt);

            const tempArt = userArt[0];
            let tempCertArt = userArt[1];
            const soldArt = userArt[2];
            const prices = userArt[3];
            if (tempCertArt[0] === "0" || tempCertArt[0] === 0) {
              tempCertArt = [];
            }

            // Set prices
            for (let i = 0; i < soldArt.length; i++) {
              artJson.map((el) => {
                if (el.id === soldArt[i]) {
                  let tempEl = el;
                  tempEl.price = prices[i];
                  return tempEl;
                }
              });
            }

            await getJsonArt(artJson, tempArt, tempCertArt);
          } else {
            setTempError("Nessun utente trovato con il tuo address");
            console.log("Check user false");
          }
        } catch (e) {
          if (e.message.includes("please set an address first")) {
            throw new Error(
              "Assicurati di aver selezionato l'RPC corretto, oppure effettua di nuovo la migrazione."
            );
          }
        }
      } else {
        console.error("ERROR LOADING THE CONTRACT");
      }
    })();
  }, []);

  // Checks if Metamask account is changed and updates UI
  useEffect(() => {
    (async function () {
      if (web3State.web3 !== null && web3State.web3 !== undefined) {
        window.ethereum.on("accountsChanged", async function (accounts) {
          console.log("CHANGING ACCOUNT!");
          let newWeb3 = web3State.web3;
          let newAccounts = await newWeb3.eth.getAccounts();
          newWeb3.eth.defaultAccount = accounts[0];
          console.log(
            web3State.web3.eth.defaultAccount === newWeb3.eth.defaultAccount
          );
          console.log("newaccounts true", newAccounts[0], accounts[0]);
          console.log("allcousntusn", accounts);
          setWeb3State({ ...web3State, accounts: newAccounts, web3: newWeb3 });
          // Update
          await updateApp(newWeb3, true);
        });
      }
    })();
  }, [web3State.web3]);

  // Displays a message when retrieving art from server or site
  const checkArtState = () => {
    switch (artState.stateFlag) {
      case 1:
        return "Recuperando le informazioni sulle opere dal server";
      case 2:
        return "Recuperando le informazioni sulle opere dal sito di aste";
      case 3:
        return (
          "Associando le informazioni alle opere, progresso: " +
          artState.currentNum +
          " su " +
          artState.totalNum
        );
      case 4:
        return "C'è stato un errore nell'ottenimento informazioni opere dal server";
      default:
        return "";
    }
  };

  // Check which page has to be rendered
  const checkState = () => {
    switch (appState) {
      case pages.HOME:
        return (
          <Main
            noArt={noArt}
            setAppState={setAppState}
            setViewArt={setViewArt}
            updateApp={updateApp}
            web3={web3State}
          />
        );
      case pages.OWNEDART:
        return (
          <OwnedArtPage
            certifiedArt={certifiedArt}
            art={art}
            setAppState={setAppState}
            setViewArt={setViewArt}
            web3={web3State}
          />
        );
      case pages.ARTPAGE:
        return (
          <Product
            art={viewArt.art}
            artType={viewArt.type}
            setAppState={setAppState}
            setViewArt={setViewArt}
            userData={userData}
            web3={web3State}
          />
        );
      case pages.REQUESTS:
        return (
          <Requests
            userData={userData}
            art={art}
            certArt={certifiedArt}
            appState={appState}
            updateApp={updateApp}
            web3={web3State}
          />
        );
      case pages.CREATEPDF:
        return (
          <CreateCertificate
            art={viewArt.art}
            userData={userData}
            updateApp={updateApp}
            web3={web3State}
          />
        );
      case pages.VALIDATEART:
        return (
          <FeedbackToast
            art={viewArt.art}
            userData={userData}
            web3={web3State}
          />
        );
      default:
        return <h3>Errore nel caricamento della pagina!</h3>;
    }
  };

  // Sets the user data from the login page
  const setLoginUser = async (name) => {
    const { contract, accounts } = web3State;

    await contract.methods
      .insertNewUser(accounts[0], name)
      .send({ from: accounts[0] });

    // setUserData({
    //   name: name,
    //   address: web3State.accounts[0],
    //   token: document.cookie
    //     .split("; ")
    //     .find((row) => row.startsWith("token"))
    //     .split("=")[1],
    // });

    await updateApp(web3State.web3, false);
  };

  /*
  Controlla se userData contiene il nome, in tal caso l'utente
  è loggato tramite Web3 e viene mostrata la home page,
  altrimenti viene mostrata la pagina di registrazione.
  */
  const checkUser = () => {
    if (userData.name) {
      return (
        <div>
          <NavBar
            appState={appState}
            setAppState={setAppState}
            userData={userData}
          />
          <p className="text-center text-danger">{error}</p>
          <main role="main" className="container starter-template">
            <h5>{checkArtState()}</h5>
            {artState.stateFlag !== 0 ? (
              <div className="">
                <div className="loader" />
              </div>
            ) : (
              ""
            )}
            {checkState()}
          </main>
        </div>
      );
    } else {
      return (
        <div data-testid="register">
          <p className="text-center text-danger">{error}</p>
          <Login setName={setLoginUser} />
        </div>
      );
    }
  };

  return checkUser();
}

export default App;
