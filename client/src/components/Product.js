/*
This software is distributed under MIT/X11 license

Copyright (c) 2021 Federico Sanna - University of Cagliari

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

import React, { Fragment, useState, useEffect } from "react";
import axios from "axios";
import Modal from "react-bootstrap/Modal";
import { pages } from "./PageEnum";
import FeedbackToast from "./FeedbackToast";
import parseError from "../utilities/web3ErrorParser";

function Product(props) {
  const [images, setImages] = useState([]);
  const [prodState, setProdState] = useState(0);
  const [certData, setCertData] = useState({});

  // Modal States
  const [showModal, setShowModal] = useState(false);
  const [showEtherscanModal, setShowEtherscanModal] = useState(false);
  const [chainName, setChainName] = useState("");
  const [value, setValue] = useState("");
  const [inputError, setInputError] = useState("");

  // Toast states
  const [hashToast, setHashToast] = useState(false);
  const [toastMessage, setToastMessage] = useState({
    msg: "",
    type: "",
  });

  // Modal Functions
  const handleClose = () => setShowModal(false);
  const handleShow = () => {
    setValue("");
    setInputError("");
    setShowModal(true);
  };

  // Etherscan Modal Functions
  const checkChainID = () => {
    return props.web3.web3.eth.getChainId().then((chainID) => {
      console.log("Chain ID is: ", chainID);
      switch (chainID) {
        case 3:
          return "ropsten";
        case 4:
          return "rinkeby";
        case 5:
          return "goerli";
        case 42:
          return "kovan";
        default:
          return "";
      }
    });
  };

  const handleCloseEthMod = () => setShowEtherscanModal(false);
  const handleShowEthMod = () => {
    setShowEtherscanModal(true);
  };

  // Toast functions
  const toggleShowToast = () => {
    setHashToast(!hashToast);
    setToastMessage({ msg: "", type: "" });
  };

  const sendRequest = async (price) => {
    console.log("Your price is: ", price);
    if (parseInt(price) < 1) {
      setInputError("Hai inserito un valore non corretto");
      return;
    }
    handleClose();
    try {
      console.log("Accounts:", props.web3.accounts);
      await props.web3.contract.methods
        .requestArt(props.art.id, price)
        .send({ from: props.web3.accounts[0] });
    } catch (e) {
      let error = parseError(e.message);
      if (error === "You already requested this art") {
        setToastMessage({
          msg: "Hai già richiesto questa opera!",
          type: "err",
        });
        setHashToast(true);
      }
    }
  };

  // Sets images
  useEffect(() => {
    const urlCreator = window.URL || window.webkitURL;
    setProdState(1);
    axios
      .get("http://localhost:9000/getimg", {
        params: { id: props.art.id, numImages: props.art.numImages },
      })
      .then((response) => {
        const imagesArray = response.data.files;
        if (Array.isArray(imagesArray)) {
          let urlArray = [];
          for (const image of imagesArray) {
            let arrayBufferView = new Uint8Array(image.data);
            let blob = new Blob([arrayBufferView], { type: "image/jpeg" });
            let url = urlCreator.createObjectURL(blob);
            urlArray.push(url);
          }
          setImages(urlArray);
          setProdState(0);
        } else {
          console.error("Error in getting the images!");
          setProdState(2);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  // Gets certified art additional data
  useEffect(() => {
    if (props.artType === "certified") {
      (async function () {
        let response = await props.web3.contract.methods
          .getCertifiedArtData(props.art.id)
          .call({ from: props.web3.accounts[0] });
        setCertData({
          certifierName: response[0],
          certifierAddress: response[1],
          pdfLink: response[2],
          pdfHash: response[3],
        });
      })();
    }
  }, []);

  // Set chain ID
  useEffect(() => {
    checkChainID().then((chainName) => setChainName(chainName));
    // console.log("Chain name is ", chainName);
  }, []);

  // Render image status as paragraph
  const imagesStatus = () => {
    if (prodState === 1) {
      return <h5>Sto prendendo le immagini dal server...</h5>;
    } else if (prodState === 2) {
      return <h5>Errore nell'ottenimento delle immagini dal server!</h5>;
    } else {
      return <Fragment />;
    }
  };

  // Calculates link for Etherscan
  const createEtherscanLink = () => {
    const contractAddress = props.web3.contract.options.address;
    const link = `https://${chainName}.etherscan.io/address/${contractAddress}#code`;
    return (
      <a target="_blank" href={link}>
        {link}
      </a>
    );
  };

  /*const getImages = (urlCreator, array, iter) => {
    return new Promise((resolve, reject) => {
      let fileName = props.art.id;
      if (iter !== 1) {
        fileName += `_${iter}`;
      }
      axios
        .get("http://localhost:9000/getfile", {
          params: { fileName: fileName + ".jpg" },
          headers: { authorization: props.userData.token },
        })
        .then((res) => {
          let arrayBufferView = new Uint8Array(res.data.file.data);
          let blob = new Blob([arrayBufferView], { type: "image/jpeg" });
          let imageUrl = urlCreator.createObjectURL(blob);
          array.push(imageUrl);
          console.log(array);
          if (iter !== props.art.numImages) {
            resolve(getImages(urlCreator, array, iter + 1));
          } else {
            resolve(array);
          }
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  };*/

  const checkArtType = () => {
    const enterCertificate = () => {
      props.setAppState(pages.CREATEPDF);
      props.setViewArt({ art: props.art, type: props.artType });
    };

    const checkAuthenticity = () => {
      if (certData && certData.pdfLink && certData.pdfHash) {
        axios
          .post("http://localhost:9000/verifyhash", {
            id: props.art.id,
            link: certData.pdfLink,
            hash: certData.pdfHash,
          })
          .then((response) => {
            // Check if status === "ok"
            if (response.data.status === "ok") {
              setToastMessage({
                msg: "Il file PDF ha un hash corretto!",
                type: "ok",
              });
            } else {
              setToastMessage({
                msg: "Errore! Gli hash non corrispondono!",
                type: "err",
              });
            }
            setHashToast(true);
          })
          .catch((err) => {
            console.log(err);
          });
      }
    };

    const downloadPdf = () => {
      if (certData && certData.pdfLink) {
        window.open(
          "http://localhost:9000/getgeneratedpdf?id=" +
            props.art.id +
            "&link=" +
            certData.pdfLink,
          "_blank"
        );
      }
    };

    const requestArt = () => {
      handleShow();
    };

    switch (props.artType) {
      case "certified":
        return (
          <Fragment>
            <div className="container mt-3">
              <h3>Nome del certificatore:</h3>
              <p>{certData.certifierName}</p>
              <h3>Address del certificatore:</h3>
              <p>{certData.certifierAddress}</p>
              <h3>Link al PDF:</h3>
              <a target="_blank" href={certData.pdfLink}>
                {certData.pdfLink}
              </a>
            </div>
            <div className="container mt-3">
              <button
                className="btn btn-success"
                onClick={() => {
                  checkAuthenticity();
                }}
              >
                Verifica autenticità
              </button>
              <button
                className="btn btn-secondary ml-3"
                onClick={async () => {
                  downloadPdf();
                }}
              >
                Scarica PDF
              </button>
            </div>
            <div className="container mt-3">
              <button
                className="btn btn-success ml-3"
                onClick={() => {
                  handleShowEthMod();
                }}
              >
                Mostra su Etherscan
              </button>
            </div>
          </Fragment>
        );
      case "owned":
        return (
          <div className="container mt-3">
            <button
              className="btn btn-danger"
              onClick={() => {
                enterCertificate();
              }}
            >
              Certifica opera
            </button>
          </div>
        );
      case "notowned":
        return (
          <div className="container mt-3">
            <button
              className="btn btn-success"
              onClick={() => {
                requestArt();
              }}
            >
              Richiedi opera
            </button>
          </div>
        );
    }
  };

  const checkArt = () => {
    if (props.art) {
      return (
        <Fragment>
          <div className="container">
            <div className="row">
              <div className="col-sm">
                <h3>Nome dell'opera:</h3>
                <p>{props.art.title}</p>
              </div>
              <div className="col-sm">
                <h3>Prezzo minimo dell'opera:</h3>
                <p>€ {props.art.minEstimate}</p>
                <h3>Prezzo massimo dell'opera:</h3>
                <p>€ {props.art.maxEstimate}</p>
              </div>
            </div>
          </div>
          <div className="container">
            <h3>Descrizione dell'opera:</h3>
            <p>{props.art.description}</p>
          </div>
          <div className="container">
            <h3>Ultimo prezzo di vendita dell'opera:</h3>
            <p>
              {props.art.price
                ? "€ " + props.art.price
                : "Attualmente invenduta"}
            </p>
          </div>
          {imagesStatus()}
          {images.length === 0 ? (
            <Fragment />
          ) : (
            <span>
              {images.map((image, imgIndex) => {
                return (
                  <img
                    src={image}
                    alt="prodimage"
                    className="h-25 w-25 ml-2 mr-2"
                    key={imgIndex}
                  />
                );
              })}
            </span>
          )}
          {checkArtType()}
        </Fragment>
      );
    } else
      return <h3 className="h3">Errore nella visualizzazione dell'opera!</h3>;
  };

  return (
    <Fragment>
      {checkArt()}
      <Modal show={showModal} onHide={handleClose} size="lg">
        <Modal.Header>
          <Modal.Title>
            Stai richiedendo l'opera all'attuale possessore
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>{props.art !== null ? props.art.title : ""}</p>
          <p>Inserisci il tuo prezzo in Euro</p>
          <input
            type="number"
            value={value}
            min="1"
            onChange={(e) => {
              setValue(e.target.value);
            }}
          />
          <p className="text-danger">{inputError}</p>
        </Modal.Body>
        <Modal.Footer>
          <button
            className="btn btn-danger"
            onClick={() => {
              handleClose();
            }}
          >
            Chiudi
          </button>
          <button
            className="btn btn-primary"
            onClick={() => {
              sendRequest(value);
            }}
          >
            Invia richiesta
          </button>
        </Modal.Footer>
      </Modal>
      <Modal show={showEtherscanModal} onHide={handleCloseEthMod} size="lg">
        <Modal.Header>
          <Modal.Title>Visualizza il contratto su Etherscan</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Per visualizzare il contratto contenente quest'opera d'arte su
            Etherscan entra nel seguente link:
          </p>
          {createEtherscanLink()}
          <p>
            Se preferisci, puoi verificare l'autenticità dell'opera manualmente!
            Entra nel contratto su Etherscan, vai nella sezione{" "}
            <i>Read Contract </i> e assicurati di aver connesso il tuo wallet al
            sito di Etherscan. Dopodichè cerca la funzione denominata{" "}
            <i>getHash </i> e inserisci nel campo ID il seguente identificatore:
          </p>
          <h5>{props.art.id}</h5>
          <p>
            Ti verrà restituito l'hash del PDF caricato sulla Blockchain. Potrai
            usarlo per verificarne l'uguaglianza con l'hash del PDF presente su
            DropBox!
          </p>
        </Modal.Body>
        <Modal.Footer>
          <button
            className="btn btn-danger"
            onClick={() => {
              handleCloseEthMod();
            }}
          >
            Chiudi
          </button>
        </Modal.Footer>
      </Modal>
      <FeedbackToast
        message={toastMessage}
        show={hashToast}
        toggleShow={toggleShowToast}
      />
    </Fragment>
  );
}

export default Product;
